;;; .ent.el --- local ent config file -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; project settings
(setq ent-project-home (file-name-directory (if load-file-name load-file-name buffer-file-name)))
(setq ent-project-name "dotadmin")
(setq ent-clean-regexp "~$\\|\\.tex$")
(setq ent-project-orgfile "README.org")

(setq ubuntu-image "dotadmin/ubuntu")
(setq manjaro-image "dotadmin/manjaro")
(setq gitlab-registry "registry.gitlab.com/dpom/")

(setq config-file (expand-file-name ".file/.config/emacs/config.el"
                                    ent-project-home))

(defun lint-emacs ()
  (with-temp-buffer
    (insert-file-contents config-file)
    (check-parens)))


(require 'ent)

(ent-tasks-init)

(task 'init  '() "initialization" '(lambda (&optional x) "bin/init-admin"))

(task 'arch  '() "install archlinux packages" '(lambda (&optional x) "bin/arch-admin"))

(task 'ubuntu  '() "install ubuntulinux packages" '(lambda (&optional x) "sudo bin/ubuntu-admin"))

(task 'generate  '() "generate admin files" '(lambda (&optional x) "bin/generate-admin"))

(task 'sync  '() "sync admin files " '(lambda (&optional x) "bin/sync-admin"))

(task 'runner-archlinux '() "test archlinux runner task " '(lambda (&optional x) "gitlab-runner exec docker test-archlinux"))

(task 'runner-ubuntu '() "test ubuntu runner task " '(lambda (&optional x) "gitlab-runner exec docker test-ubuntu"))

(task 'check '(generate) "check config" '(lambda (&optional x) (ent-emacs "lint-emacs"
                                                         (expand-file-name ".ent.el" ent-project-home))))

(provide '.ent)
;;; .ent.el ends here

;; Local Variables:
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
