
* 2021-07-23

- migrarea org-roam v1->v2:
  - replace #+ROAM_KEY with ROAM_REF
  - replace #+ROAM_ALIAS with ROAM_ALIASES
  - move #+ROAM_TAGS into #+FILETAGS property

* 2021-11-06

- migrare catre guix folosind https://github.com/daviwil/dotfiles
-

* 2021-11-09

- "mi-am prins urechile" in refactorizare asa ca incep din nou cu un fisier cat mai simplu urmand ulterior sa adaug features.

* 2023-05-04

- am terminat de mutat de la evil la meow, acum doar trebuie sa ma obisnuiesc cu noua paradigma (selectie -> operatie)
