;;; .ent.el --- local ent config file -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; project settings
(setq ent-project-home (file-name-directory (if load-file-name load-file-name buffer-file-name)))
(setq ent-project-name "corgi")
(setq ent-clean-regexp "~$\\|\\.tex$")
(setq ent-project-orgfile "../README.org")

(defun get-string-from-file (filePath)
  "Return FILEPATH's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

;; (setq image-name (get-string-from-file  (expand-file-name ent-project-home "Version")))
(setq image-name (get-string-from-file  (concat ent-project-home "Version")))

(require 'ent)

(ent-tasks-init)

(task 'version  '() "display image name/version" '(lambda (&optional x) (concat "echo image: " image-name) ))


(task 'build  '() "build image" '(lambda (&optional x) (concat "docker build -t "
                                                               image-name " .")))


(task 'lint  '() "lint dockerfile" '(lambda (&optional x) "docker run -it --rm -v \"$PWD/Dockerfile\":/Dockerfile:ro replicated/dockerfilelint:09a5034 /Dockerfile"))

(task 'deploy '() "deploy image to dockerhub" '(lambda (&optional x) (concat "docker push " image-name)))

(task 'packemacs  '() "package emacs.d" '(lambda (&optional x) "../../bin/build-emacsd.sh" ))

(task 'restoremacs  '() "restore emacs.d" '(lambda (&optional x) "../../bin/restore-emacsd.sh" ))

(provide '.ent)
;;; .ent.el ends here

;; Local Variables:
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
