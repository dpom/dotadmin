#!/usr/bin/env sh

docker push "$1:$2"
docker push "$1:latest"
