#!/usr/bin/env bash
# the base image alocates already my uid and gid
USER_UID=1001
USER_GID=1001
USER_NAME=$USER

docker build --build-arg USER="$USER_NAME" \
	--build-arg USER_UID="$USER_UID" \
	--build-arg USER_GID="$USER_GID" \
	-t "$1:$2" \
  -t "$1:latest" \
  .
