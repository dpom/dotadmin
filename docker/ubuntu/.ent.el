;;; .ent.el --- local ent config file -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; project settings
(setq ent-project-home (file-name-directory (if load-file-name load-file-name buffer-file-name)))
(setq ent-project-name "dotadmin/ubuntu")
(setq ent-clean-regexp "~$\\|\\.tex$")

(setq gitlab-registry "registry.gitlab.com/dpom/")

(defun get-string-from-file (filePath)
  "Return FILEPATH's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(setq image-name (concat ent-project-name ":" (get-string-from-file  (concat ent-project-home "Version"))))

(require 'ent)

(ent-tasks-init)

(task 'image  '() "display image name/version" '(lambda (&optional x) (concat "echo image: " image-name) ))

(task 'lint  '() "lint dockerfile" '(lambda (&optional x) "docker run -it --rm -v \"$PWD/Dockerfile\":/Dockerfile:ro replicated/dockerfilelint:09a5034 /Dockerfile"))

(task 'build  '() "build image" '(lambda (&optional x) (concat "bash build.sh " image-name)))

(task 'deploy '() "deploy docker image " '(lambda (&optional x) (concat "docker tag "
                                                                                      image-name
                                                                                      " "
                                                                                      gitlab-registry
                                                                                      image-name
                                                                                      ";"
                                                                                      "docker push "
                                                                                      gitlab-registry
                                                                                      image-name)))

(provide '.ent)
;;; .ent.el ends here

;; Local Variables:
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
