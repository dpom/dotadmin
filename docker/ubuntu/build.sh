#!/usr/bin/env bash

USER_UID=$(id -u)
USER_GID=$(id -g)

docker build --build-arg USER="$USER" \
	--build-arg USER_UID="$USER_UID" \
	--build-arg USER_GID="$USER_GID" \
	-t "$1" \
	.
