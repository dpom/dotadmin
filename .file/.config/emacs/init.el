;;; init.el --- user init file  -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; Make startup faster by reducing the frequency of garbage
;; collection.
(setq gc-cons-threshold (* 50 1000 1000))

;; Load the "real" config file
(load-file (expand-file-name "config.el" user-emacs-directory))

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

;;; init.el ends here
(put 'narrow-to-region 'disabled nil)
