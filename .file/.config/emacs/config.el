;; config.el -- user customization file -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)

(require 'cl-lib)

(cl-defun local/vc-install (&key (host "github.com") repo name rev backend)
  "Install a package from a remote if it's not already installed.
This is a thin wrapper around `package-vc-install' in order to
make non-interactive usage more ergonomic.  Takes the following
named arguments:

- HOST the remote where to get the package (e.g., \"gitlab.com\").
  If omitted, this defaults to \"github.com\".

- REPO should be the name of the repository (e.g.,
  \"slotThe/arXiv-citation\".

- NAME, REV, and BACKEND are as in `package-vc-install' (which
  see)."
  (let* ((url (format "https://%s/%s" host repo))
         (iname (when name (intern name)))
         (pac-name (or iname (intern (file-name-base repo)))))
    (unless (package-installed-p pac-name)
      (package-vc-install url iname rev backend))))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-vc-install-selected-packages)
(package-initialize)

(use-package bug-hunter
  :config
  (global-set-key (kbd "<f12>") #'bug-hunter-init-file))

(defmacro prot-emacs-keybind (keymap &rest definitions)
  "Expand key binding DEFINITIONS for the given KEYMAP.
DEFINITIONS is a sequence of string and command pairs."
  (declare (indent 1))
  (unless (zerop (% (length definitions) 2))
    (error "Uneven number of key+command pairs"))
  (let ((keys (seq-filter #'stringp definitions))
        ;; We do accept nil as a definition: it unsets the given key.
        (commands (seq-remove #'stringp definitions)))
    `(when-let (((keymapp ,keymap))
                (map ,keymap))
       ,@(mapcar
          (lambda (pair)
            (unless (and (null (car pair))
                         (null (cdr pair)))
              `(define-key map (kbd ,(car pair)) ,(cdr pair))))
          (cl-mapcar #'cons keys commands)))))

(use-package exec-path-from-shell
  :init
  (local/vc-install :repo "purcell/exec-path-from-shell")
  :config
  (customize-set-variable 'exec-path-from-shell-variables
                        '("PATH" "MANPATH" "DPOM_CONFIG"
                          "XDG_BIN_HOME"  "XDG_CACHE_HOME"
                          "XDG_CONFIG_HOME" "XDG_DATA_HOME"
                          "XDG_LIB_HOME"))
  (customize-set-variable 'exec-path-from-shell-check-startup-files nil)
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))

(defvar xdg-bin (or (getenv "XDG_BIN_HOME") "~/.local/bin/")
  "The XDG bin base directory.")

(defvar xdg-cache (or (getenv "XDG_CACHE_HOME") "~/.cache/")
  "The XDG cache base directory.")

(defvar xdg-config (or (getenv "XDG_CONFIG_HOME") "~/.config/")
  "The XDG config base directory.")

(defvar xdg-data (or (getenv "XDG_DATA_HOME") "~/.local/share/")
  "The XDG data base directory.")

(defvar xdg-lib (or (getenv "XDG_LIB_HOME") "~/.local/lib/")
  "The XDG lib base directory.")

(use-package direnv
  :config
  (direnv-mode))

(let ((personal-settings (expand-file-name "local.el" user-emacs-directory)))
  (if (file-exists-p personal-settings)
      (progn
        (load-file personal-settings))
    (progn
      ;; default values if local file missing
      (setq calendar-date-style 'iso)
      (setq local/workspace (expand-file-name "~/workspace")
            local/pers-dir local/workspace
            local/work-dir local/workspace
            local/plan-dir local/workspace
            local/org-dir  local/workspace
            local/projects-dir  local/workspace
            local/notes-dir  local/workspace
            local/admin-dir (expand-file-name "~/.admin/")
            local/projects-dir  local/workspace
            local/resources-dir  local/workspace
            local/bibliography-library-path local/workspace)
      (setq local/dummy-file (expand-file-name "~/Dummy.org")
            local/accounts-file local/dummy-file
            local/bookmarks-file local/dummy-file
            local/contacts-file  local/dummy-file
            local/bibliography-file local/dummy-file))))

(add-to-list 'load-path (expand-file-name "emacs/elisp" xdg-config))

(customize-set-variable 'local/config-var-dir (file-name-as-directory  (expand-file-name "var" user-emacs-directory)))

;; Revert Dired and other buffers
(customize-set-variable 'global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Use "y" and "n" to confirm/negate prompt instead of "yes" and "no"
;; Using `advice' here to make it easy to reverse in custom
;; configurations with `(advice-remove 'yes-or-no-p #'y-or-n-p)'
;;
;; N.B. Emacs 28 has a variable for using short answers, which should
;; be preferred if using that version or higher.
(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (advice-add 'yes-or-no-p :override #'y-or-n-p))


;; Turn on recentf mode
(add-hook 'after-init-hook #'recentf-mode)
(customize-set-variable 'recentf-save-file
                        (expand-file-name "recentf"
                                          local/config-var-dir))

;; Do not saves duplicates in kill-ring
(customize-set-variable 'kill-do-not-save-duplicates t)

;; Make scrolling less stuttered
(setq-default
 auto-window-vscroll nil
 fast-but-imprecise-scrolling t
 scroll-conservatively 101
 scroll-margin 0
 scroll-preserve-screen-position t)

;; Better support for files with long lines
(setq-default
  bidi-paragraph-direction 'left-to-right
  bidi-inhibit-bpa t)
(global-so-long-mode 1)

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

(setq-default large-file-warning-threshold nil)

(setq-default vc-follow-symlinks t)

(setq ad-redefinition-action 'accept)

(setq
 ediff-make-buffers-readonly-at-startup nil
 ediff-show-clashes-only t
 ediff-split-window-function 'split-window-horizontally
 ediff-window-setup-function 'ediff-setup-windows-plain)

(column-number-mode)
(global-display-line-numbers-mode t)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                erc-mode-hook
                term-mode-hook
                eshell-mode-hook
                vterm-mode-hook
                neotree-mode-hook
                dashboard-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(setq browse-url-browser-function 'browse-url-default-browser)

(setq-default
 buffer-file-coding-system 'utf-8-unix
 default-file-name-coding-system 'utf-8-unix
 default-keyboard-coding-system 'utf-8-unix
 default-process-coding-system '(utf-8-unix . utf-8-unix)
 default-sendmail-coding-system 'utf-8-unix
 default-terminal-coding-system 'utf-8-unix)

(defgroup local-ui '()
  "User interface related configuration."
  :tag "Local UI"
  :group 'local)

(setq-default inhibit-startup-message t)
(scroll-bar-mode -1)  ; Disable visible scrollbar
(tool-bar-mode -1)    ; Disable the toolbar
(tooltip-mode -1)     ; Disable tooltips
(set-fringe-mode 10)  ; Give some breathing room
(menu-bar-mode 1)    ; Enable the menu bar
(setq visible-bell t) ; Set up the visible bell

(setq-default
  mouse-wheel-scroll-amount '(1 ((shift) . 1)  ((control) . nil)) ;; one line at a time
  mouse-wheel-progressive-speed nil ;; don't accelerate scrolling
  mouse-wheel-follow-mouse 't ;; scroll window under mouse
 scroll-margin 2 ; scroll with 2 line margin for continuity
;; keyboard scroll one line at a time instead of jumping
 scroll-step            1
 scroll-conservatively  10000)

;; (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
;; (add-to-list 'default-frame-alist '(alpha . (90 . 90)))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(use-package fontaine
  :config
  (setq fontaine-latest-state-file
        (locate-user-emacs-file "fontaine-latest-state.eld"))
  (setq fontaine-presets
        '((tiny
           :default-family "Iosevka"
           :default-height 70)
          (small
           :default-family "Iosevka"
           :default-height 90)
          (regular
           :default-height 100)
          (medium
           :default-height 130)
          (large
           :default-weight semilight
           :default-height 150
           :bold-weight extrabold)
          (presentation
           :default-weight semilight
           :default-height 180
           :bold-weight extrabold)
          (jumbo
           :default-weight semilight
           :default-height 220
           :bold-weight extrabold)
          (t
           ;; I keep all properties for didactic purposes, but most can be
           ;; omitted.  See the fontaine manual for the technicalities:
           ;; <https://protesilaos.com/emacs/fontaine>.
           :default-family "Iosevka"
           :default-weight regular
           :default-height 100
           :fixed-pitch-family nil ;q falls back to :default-family
           :fixed-pitch-weight nil ; falls back to :default-weight
           :fixed-pitch-height 1.0
           :fixed-pitch-serif-family nil ; falls back to :default-family
           :fixed-pitch-serif-weight nil ; falls back to :default-weight
           :fixed-pitch-serif-height 1.0
           :variable-pitch-family "Iosevka  Motion Duo"
           :variable-pitch-weight nil
           :variable-pitch-height 1.0
           :bold-family nil ; use whatever the underlying face has
           :bold-weight bold
           :italic-family "Source Code Pro"
           :italic-slant italic
           :line-spacing nil)))

  ;; Recover last preset or fall back to desired style from
  ;; `fontaine-presets'.
  (fontaine-set-preset (or (fontaine-restore-latest-preset) 'large))

  ;; The other side of `fontaine-restore-latest-preset'.
  (add-hook 'kill-emacs-hook #'fontaine-store-latest-preset)
  )

(defun local/iw-set-font-size ()
  "Adjust the font size in all windows."
  (interactive)
  (let (font-size)
    (setq font-size (read-number "Text size: "))
    (set-frame-font (font-spec :size font-size) t `(,(selected-frame)))))

(use-package ef-themes
  :config
  (load-theme 'ef-elea-dark t))

(use-package all-the-icons)

(use-package all-the-icons-completion
  :after (marginalia all-the-icons)
  :config
  (add-hook 'marginalia-mode #'all-the-icons-completion-marginalia-setup)
  (all-the-icons-completion-mode 1))

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default)
  (kind-icon-use-icons t)
  (kind-icon-blend-frac 0.08)
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package all-the-icons-dired
  :after dired
  :config
  (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

(autoload 'iimage-mode "iimage" "Support Inline image minor mode." t)
(autoload 'turn-on-iimage-mode "iimage" "Turn on Inline image minor mode." t)
;;
;; ** Display images in *Info* buffer.
;;
;; (add-hook 'info-mode-hook 'turn-on-iimage-mode)
;;

(use-package doom-modeline
  ;; Start up the modeline after initialization is finished
  :hook after-init-hook
  :custom
  (doom-modeline-height 15)
  (doom-modeline-bar-width 6)
  (doom-modeline-minor-modes t)
  (doom-modeline-icon t)
  (doom-modeline-lsp t)
  (doom-modeline-github nil)
  (doom-modeline-mu4e nil)
  (doom-modeline-irc t)
  (doom-modeline-persp-name nil)
  (doom-modeline-project-detection nil)
  (doom-modeline-unicode-fallback t)
  (doom-modeline-buffer-file-name-style 'relative-from-project))

(use-package all-the-icons
  :ensure t)

(use-package minions
  :hook doom-modeline-mode)


(custom-set-faces '(mode-line ((t (:height 0.85))))
                  '(mode-line-inactive ((t (:height 0.85)))))

(use-package diminish
  :config
  (diminish 'gcmh-mode)
  (diminish 'buffer-face-mode)
  (diminish 'eldoc-mode) )

(global-subword-mode 1)
(column-number-mode 1)      ; Show the column number
(global-hl-line-mode t)
(window-divider-mode 1)

(desktop-save-mode 1)

(customize-set-variable 'x-select-enable-clipboard-manager nil)

(use-package ansi-color
  :config
  (add-hook 'shell-mode-hook  #'ansi-color-for-comint-mode-on)
  (defun endless/colorize-compilation ()
    "Colorize from `compilation-filter-start' to `point'."
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region compilation-filter-start (point))))
  (add-to-list 'comint-output-filter-functions 'ansi-color-process-output)
  (add-hook 'compilation-filter-hook #'endless/colorize-compilation)
  (add-hook 'eshell-preoutput-filter-functions 'ansi-color-filter-apply))

;; add visual pulse when changing focus, like beacon but built-in
;; from from https://karthinks.com/software/batteries-included-with-emacs/
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command scroll-down-command
                                     recenter-top-bottom other-window))
  (advice-add command :after #'pulse-line))

(customize-set-variable 'split-width-threshold 0)
(customize-set-variable 'split-height-threshold nil)

(setq-default tab-always-indent 'complete
              completion-cycle-threshold nil
              tab-first-completion 'word-or-paren-or-punct ; Emacs 27
              tab-width 2
              indent-tabs-mode nil)
(setq completion-styles '(basic substring initials orderless) ; also see `completion-category-overrides'
      completion-category-defaults nil
      completion-category-overrides  '((file (styles . (basic partial-completion orderless)))
                                       (project-file (styles . (basic substring partial-completion orderless)))
                                       (imenu (styles . (basic substring orderless)))
                                       (kill-ring (styles . (basic substring orderless)))
                                       (consult-location (styles . (basic substring orderless))))
      completion-ignore-case t
      read-buffer-completion-ignore-case t
      read-file-name-completion-ignore-case t
      case-fold-search t
      resize-mini-windows t)

(define-key completion-list-mode-map "n" 'next-line)
(define-key completion-list-mode-map "p" 'previous-line)
(define-key completion-list-mode-map "f" 'next-completion)
(define-key completion-list-mode-map "b" 'previous-completion)
(define-key completion-list-mode-map "M-v" 'local/focus-minibuffer)
(define-key completion-list-mode-map "?" 'local/focus-minibuffer)

(use-package corfu 
  :custom
  (corfu-auto-delay nil) 
  (corfu-auto-delay 0.25) 
  (corfu-auto-prefix 2) 
  (corfu-count 14) 
  (corfu-cycle t) 
  (corfu-echo-documentation 0.25) 
  (corfu-preview-current 'insert) 
  (corfu-preselect-first nil) 
  (corfu-separator ?\s) 
  (corfu-quit-no-match 'separator) 
  (corfu-quit-at-boundary nil) 
  (corfu-min-width 80) 
  (corfu-max-width corfu-min-width) 
  (corfu-scroll-margin 4)
  :config
  (global-corfu-mode 1)
  ;; move to minibuffer
  (defun corfu-move-to-minibuffer () 
    "Move \"popup\" completion candidates to minibuffer.

Useful if you want a more robust view into the recommend candidates." 
    (interactive) 
    (let (completion-cycle-threshold completion-cycling) 
      (apply #'consult-completion-in-region completion-in-region--data)))
  ;; Enable Corfu more generally for every minibuffer, as long as no other
  ;; completion UI is active. If you use Mct or Vertico as your main minibuffer
  ;; completion UI. From
  ;; https://github.com/minad/corfu#completing-with-corfu-in-the-minibuffer
  (defun corfu-enable-always-in-minibuffer () 
    "Enable Corfu in the minibuffer if Vertico/Mct is not active."
    (unless (bound-and-true-p vertico--input) 
      (setq-local corfu-auto nil) ; Ensure auto completion is disabled
      (corfu-mode 1)))
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1)
  (define-key corfu-map "<return>" nil)
  (define-key corfu-map "<tab>" 'corfu-complete)
  (define-key corfu-map "<backtab>" 'corfu-previous)
  (define-key corfu-map "<escape>" 'corfu-quit)
  (define-key corfu-map "C-h" 'corfu-show-documentation)
  (define-key corfu-map "M-." 'corfu-show-location)
  (define-key corfu-map "M-m" 'corfu-move-to-minibuffer)
  (define-key corfu-map "M-p" 'corfu-popupinfo-scroll-down)
  (define-key corfu-map "M-n" 'corfu-popupinfo-scroll-up)
  (define-key corfu-map "M-d" 'corfu-popupinfo-toggle)
  )

(use-package dabbrev
  ;; Swap M-/ and C-M-/
  ;; :bind (("M-/" . dabbrev-completion)
  ;;        ("C-M-/" . dabbrev-expand))
  ;; Other useful Dabbrev configurations.
  :custom
  (dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'")))

(use-package company) ;; for yasnippet

(use-package cape
  :config
  ;; Add useful defaults completion sources from cape
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;; (add-to-list 'completion-at-point-functions #'cape-symbol)

    ;; LSP
;;   (with-eval-after-load 'lsp-mode
;;     (add-hook 'lsp-completion-mode-hook  #'local/cape-capf-setup-lsp)
;;     (defun local/cape-capf-setup-lsp ()
;;       "Replace the default `lsp-completion-at-point' with its
;; `cape-capf-buster' version. Also add `cape-file' and
;; `company-yasnippet' backends."
;;       (setf (elt (cl-member 'lsp-completion-at-point completion-at-point-functions) 0)
;;             (cape-capf-buster #'lsp-completion-at-point))
;;       (require 'company-yasnippet)
;;       (add-to-list 'completion-at-point-functions (cape-company-to-capf #'company-yasnippet))
;;       (add-to-list 'completion-at-point-functions #'cape-dabbrev t)))

  ;; Org
  (with-eval-after-load 'org
    (add-hook 'org-mode-hook #'local/cape-capf-setup-org)
    (defun local/cape-capf-setup-org ()
      (require 'org-roam)
      (require 'company-yasnippet)
      (add-to-list 'completion-at-point-functions  (cape-super-capf #'cape-dabbrev (cape-company-to-capf #'company-yasnippet)))
      (add-to-list 'completion-at-point-functions #'org-roam-complete-link-at-point)))
  
  (add-hook 'eshell-mode-hook
            (lambda () (setq-local corfu-quit-at-boundary t
                                   corfu-quit-no-match t
                                   corfu-auto nil)
              (corfu-mode))))

;;;###autoload
(defun local/orderless-literal-dispatcher (pattern _index _total)
  (when (string-suffix-p "=" pattern)
    `(orderless-literal . ,(substring pattern 0 -1))))


;;;###autoload
(defun local/orderless-flex-dispatcher (pattern _index _total)
  (when (or (string-suffix-p "`" pattern)
            (string-suffix-p "~" pattern))
    `(orderless-flex . ,(substring pattern 0 -1))))


;;;###autoload
(defun local/orderless-exclude-dispatcher (pattern _index _total)
  (when (string-prefix-p "!" pattern)
    `(orderless-without-literal . ,(substring pattern 1))))

(use-package orderless
  :custom
  (completion-styles '(orderless partial-completion))
  (completion-category-overrides '((file (styles . (partial-completion)))))
  (orderless-component-separator " +")
  (orderless-matching-styles '(orderless-literal
                               orderless-prefixes
                               orderless-flex
                               orderless-regexp))
  (orderless-style-dispatchers '(local/orderless-literal-dispatcher
                                 local/orderless-flex-dispatcher)))

;; (use-package orderless
;;   :ensure t
;;   :custom
;;   (completion-styles '(orderless basic))
;;   (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package consult)

(use-package embark
  :custom
  (embark-prompter 'embark-completing-read-prompter)
  :config
  (global-set-key [remap describe-bindings] #'embark-bindings)
  (setq prefix-help-command #'embark-prefix-help-command)

  (defun local/embark-act-no-quit ()
    "Call `embark-act' but do not quit after the action."
    (interactive)
    (let ((embark-quit-after-action nil))
      (call-interactively #'embark-act)))

  (defun local/embark-act-quit ()
    "Call `embark-act' and quit after the action."
    (interactive)
    (let ((embark-quit-after-action t))
      (call-interactively #'embark-act))
    (when (and (> (minibuffer-depth) 0)
               (derived-mode-p 'completion-list-mode))
      (abort-recursive-edit)))

  (dolist (map (list global-map embark-collect-mode-map minibuffer-local-filename-completion-map))
    (define-key map (kbd "C-,") #'prot/embark-act-no-quit)
    (define-key map (kbd "<f6>") #'prot/embark-act-no-quit)
    (define-key map (kbd "C-.") #'prot/embark-act-quit)
    (define-key map (kbd "<f5>") #'prot/embark-act-quit))

  (global-set-key (kbd "M-.") #'embark-dwim)
  (global-set-key (kbd "<f7>") #'embark-dwim)
  (global-set-key (kbd "C-;") #'embark-dwim)
  (defmacro local/embark-ace-action (fn)
    `(defun ,(intern (concat "local/embark-ace-" (symbol-name fn))) ()
       (interactive)
       (with-demoted-errors "%s"
         (require 'ace-window)
         (let ((aw-dispatch-always t))
           (aw-switch-to-window (aw-select nil))
           (call-interactively (symbol-function ',fn))))))
  (define-key embark-file-map     (kbd "o") (local/embark-ace-action find-file))
  (define-key embark-file-map (kbd "S") 'local/sudo-find-file)
  (define-key embark-buffer-map   (kbd "o") (local/embark-ace-action switch-to-buffer))
  )

(use-package embark-consult
  :after (embark consult)
  :config
  (add-hook 'embark-collect-mode-hook #'consult-preview-at-point-mode))

(use-package marginalia
  :custom
  (marginalia-align 'right)
  (marginalia-max-relative-age 0)
  :config
  (marginalia-mode 1)
  )

(use-package vertico
  :init
  (local/vc-install :repo "minad/vertico")
  :custom
  (vertico-scroll-margin 0)
  (vertico-count 5)
  (vertico-resize nil)
  (vertico-cycle t)
  (vertico-grid-separator "       ")
  (vertico-grid-lookahead 50)
  (vertico-buffer-display-action '(display-buffer-reuse-window))
  ;; (vertico-multiform-categories '((file reverse)
  ;;                                 (consult-grep buffer)

  ;;                                 (imenu buffer)
  ;;                                 (library reverse indexed)
  ;;                                 (org-roam-node reverse indexed)
  ;;                                 (t reverse)))
  (vertico-multiform-commands '(("flyspell-correct-*" grid reverse)
                                (org-refile grid reverse indexed)
                                (consult-yank-pop indexed)
                                (consult-flycheck)
                                (consult-lsp-diagnostics)))
  :config
  (vertico-mode 1)
  (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)

  (defvar prot-vertico-minimal
    '(unobtrusive
      (vertico-flat-format . ( :multiple  ""
                               :single    ""
                               :prompt    ""
                               :separator ""
                               :ellipsis  ""
                               :no-match  "")))
    "List of configurations for minimal Vertico multiform.
The minimal view is intended to be more private or less
revealing.  This is important when, for example, a prompt shows
names of people.  Of course, such a view also provides a minimal
style for general usage.

Toggle the vertical view with the `vertico-multiform-vertical'
command or use the commands `prot-vertico-private-next' and
`prot-vertico-private-previous', which toggle the vertical view
automatically.")

  (defvar prot-vertico-maximal
    '((vertico-count . 10)
      (vertico-resize . t))
    "List of configurations for maximal Vertico multiform.")

  ;; Sort directories before files.  From the Vertico documentation.
  (defun contrib/sort-directories-first (files)
    (setq files (vertico-sort-history-length-alpha files))
    (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
           (seq-remove (lambda (x) (string-suffix-p "/" x)) files)))

  (setq vertico-multiform-categories
        `(;; Maximal
          (embark-keybinding ,@prot-vertico-maximal)
          (multi-category ,@prot-vertico-maximal)
          (consult-location ,@prot-vertico-maximal)
          (imenu ,@prot-vertico-maximal)
          (unicode-name ,@prot-vertico-maximal)
          ;; Minimal
          (file ,@prot-vertico-minimal
                (vertico-preselect . prompt)
                (vertico-sort-function . contrib/sort-directories-first))
          (t ,@prot-vertico-minimal)))

  (vertico-multiform-mode 1)

  (defun prot-vertico-private-next ()
    "Like `vertico-next' but toggle vertical view if needed.
This is done to accommodate `prot-vertico-minimal'."
    (interactive)
    (if vertico-unobtrusive-mode
        (let ((vertico--index 0))
          (vertico-multiform-vertical)
          (vertico-next 1))
      (vertico-next 1)))

  (defun prot-vertico-private-previous ()
    "Like `vertico-previous' but toggle vertical view if needed.
This is done to accommodate `prot-vertico-minimal'."
    (interactive)
    (if vertico-unobtrusive-mode
        (progn
          (vertico-multiform-vertical)
          (vertico-previous 1))
      (vertico-previous 1)))

  (defun prot-vertico-private-complete ()
    "Expand contents and show remaining candidates, if needed.
This is done to accommodate `prot-vertico-minimal'."
    (interactive)
    (if (and vertico-unobtrusive-mode (> vertico--total 1))
        (progn
          (minibuffer-complete)
          (vertico-multiform-vertical))
      (vertico-insert)))

  (defun local/vertico-multiform-flat-toggle ()
    "Toggle between flat and reverse."
    (interactive)
    (vertico-multiform--display-toggle 'vertico-flat-mode)
    (if vertico-flat-mode
        (vertico-multiform--temporary-mode 'vertico-reverse-mode -1)
      (vertico-multiform--temporary-mode 'vertico-reverse-mode 1)))
  
  (defun local/vertico-quick-embark (&optional arg)
    "Embark on candidate using quick keys."
    (interactive)
    (when (vertico-quick-jump)
      (embark-act arg)))

  ;; Workaround for problem with `tramp' hostname completions. This overrides
  ;; the completion style specifically for remote files! See
  ;; https://github.com/minad/vertico#tramp-hostname-completion
  (defun local/basic-remote-try-completion (string table pred point)
    (and (vertico--remote-p string)
         (completion-basic-try-completion string table pred point)))
  (defun local/basic-remote-all-completions (string table pred point)
    (and (vertico--remote-p string)
         (completion-basic-all-completions string table pred point)))
  (add-to-list 'completion-styles-alist
               '(basic-remote           ; Name of `completion-style'
                 local/basic-remote-try-completion local/basic-remote-all-completions nil))
  (require 'vertico-directory)
  (add-hook 'rfn-eshadow-update-overlay #'vertico-directory-tidy) ; Clean up file path when typing
  (add-hook 'minibuffer-setup #'vertico-repeat-save) ; Make sure vertico state is saved
  ;; Prefix the current candidate with “» ”. From
  ;; https://github.com/minad/vertico/wiki#prefix-current-candidate-with-arrow
  ;; (advice-add #'vertico--format-candidate :around
  ;;             (lambda (orig cand prefix suffix index _start)
  ;;               (setq cand (funcall orig cand prefix suffix index _start))
  ;;               (concat
  ;;                (if (= vertico--index index)
  ;;                    (propertize "» " 'face 'vertico-current)
  ;;                  "  ")
  ;;                cand)))
  ;; This works with `file-name-shadow-mode'.  When you are in a
  ;; sub-directory and use, say, `find-file' to go to your home '~/' or
  ;; root '/' directory, Vertico will clear the old path to keep only
  ;; your current input.
  (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)
  (prot-emacs-keybind vertico-map
    "<backspace>"   #'vertico-directory-delete-char
    "<escape>"      #'minibuffer-keyboard-quit
    "<tab>"         #'prot-vertico-private-complete
    "?"             #'minibuffer-completion-help
    "C-<backspace>" #'vertico-directory-delete-word
    "C-M-n"         #'vertico-next-group
    "C-M-p"         #'vertico-previous-group
    "C-i"           #'vertico-quick-insert
    "C-j"           #'vertico-next
    "C-k"           #'vertico-previous
    "C-l"           #'local/vertico-multiform-flat-toggle
    "C-o"           #'vertico-quick-exit
    "C-w"           #'vertico-directory-delete-word
    "M-F"           #'vertico-multiform-flat
    "M-G"           #'vertico-multiform-grid
    "M-R"           #'vertico-multiform-reverse
    "M-RET"         #'minibuffer-force-complete-and-exit
    "M-TAB"         #'minibuffer-complete
    "M-U"           #'vertico-multiform-unobtrusive
    "M-h"           #'vertico-directory-up
    "M-o"           #'local/vertico-quick-embark)
)

(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
        #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

(require 'minibuffer)
(setq enable-recursive-minibuffers t
      minibuffer-eldef-shorten-default t
      read-answer-short t
      echo-keystrokes 0.25
      kill-ring-max 60)
(defun basic-remote-try-completion (string table pred point)
  (and (path-remote-p string)
       (completion-basic-try-completion string table pred point)))
(defun basic-remote-all-completions (string table pred point)
  (and (path-remote-p string)
       (completion-basic-all-completions string table pred point)))
;; (add-to-list
;;  'completion-styles-alist
;;  '(basic-remote basic-remote-try-completion basic-remote-all-completions nil))

;;   (defun path-remote-p (path)
;;     "Return t if PATH is a remote path."
;;     (string-match-p "\\`/[^/|:]+:" (substitute-in-file-name path)))

;;   (defun local/messageless (fn &rest args)
;;     "Set `minibuffer-message-timeout' to 0.
;; Meant as advice around minibuffer completion FN with ARGS."
;;     (let ((minibuffer-message-timeout 0))
;;       (apply fn args)))

;;   (advice-add 'minibuffer-force-complete-and-exit :around #'local/messageless)

;; (define-key minibuffer-local-completion-map (kbd "-") #'minibuffer-complete-word)

(defun local/minibuffer-focus-mini ()
  "Focus the active minibuffer."
  (interactive)
  (let ((mini (active-minibuffer-window)))
    (when mini
      (select-window mini))))

;; Try really hard to keep the cursor from getting stuck in the read-only prompt
;; portion of the minibuffer.
(setq minibuffer-prompt-properties
      '(read-only t intangible t cursor-intangible t face minibuffer-prompt))

(add-hook 'minibuffer-setup #'cursor-intangible-mode)

;; Also adapted from Vertico.
(defun crm-indicator (args)
  "Add prompt indicator to `completing-read-multiple' filter ARGS."
  ;; The `error' face just makes the text red.
  (cons (concat (propertize "[CRM] " 'face 'error) (car args)) (cdr args)))

(advice-add #'completing-read-multiple :filter-args #'crm-indicator)

(minibuffer-depth-indicate-mode 1)
(minibuffer-electric-default-mode 1)
(file-name-shadow-mode 1)

(use-package savehist
  :custom
   (savehist-file (expand-file-name "history" local/config-var-dir))
   (history-length 500)
   (history-delete-duplicates t)
   (savehist-save-minibuffer-history t)
   (savehist-additional-variables '(register-alist kill-ring))
  :hook after-init
  :config (savehist-mode 1))

(use-package yasnippet
  :custom
  (yas-snippet-dirs  (list (file-name-as-directory (locate-user-emacs-file "snippets"))))
  :config
  (yas-global-mode 1))

(autoload 'goto-last-change "goto-last-change"
	"Set point to the position of the last change." t)

(defvar *protected-buffers* '("*scratch*" "*Messages*")
  "Buffers that cannot be killed.")

(defun local/protected-buffers ()
  "Protects some buffers from being killed."
  (dolist (buffer *protected-buffers*)
    (with-current-buffer buffer
      (emacs-lock-mode 'kill))))

(add-hook 'after-init-hook #'local/protected-buffers)

(use-package popper
  :init
  (local/vc-install :repo "karthink/popper")
  :custom
  (popper-window-height 12)
  (popper-reference-buffers '("^\\*eshell\\*"
                              "^vterm"
                              "\\*Messages\\*"
                              "Output\\*$"
                              help-mode
                              helpful-mode
                              compilation-mode))
  :config
  (require 'popper) ;; Needed because I disabled autoloads
  (popper-mode 1))

(require 'midnight)

(customize-set-variable 'undo-tree-auto-save-history nil)

(defmacro make-find (functionname filename)
  (let ((funsymbol (intern (concat "find-" functionname))))
    `(defun ,funsymbol () (interactive) (find-file  ,filename))))

(make-find "config" local/config-file)
(make-find "accounts" local/accounts-file)
(make-find "contacts" local/contacts-file)

(defun find-notebook ()
  (interactive)
  (let ((dir (locate-dominating-file default-directory "Notebook.org")))
    (if dir
        (find-file (expand-file-name "Notebook.org"  dir))
      (message "No Notebook"))
    ))

(defun local/search-in-files ()
  (interactive)
  (consult-grep t))

(require 'dired)
(setq dired-recursive-copies 'always
  dired-recursive-deletes 'always
  dired-isearch-filenames 'dwim
  dired-hide-details-hide-symlink-targets nil
  delete-by-moving-to-trash t
  dired-auto-revert-buffer t
  dired-listing-switches "-AFhlv --group-directories-first"
  dired-dwim-target t)
(file-name-shadow-mode 1)
(autoload 'dired-async-mode "dired-async.el" nil t)

(add-hook 'dired-mode-hook #'dired-hide-details-mode)
(add-hook 'dired-mode-hook #'hl-line-mode)

(use-package async
  :config
  (dired-async-mode 1))

(use-package dired-subtree
  ;:bind
  ;(:map dired-mode-map
        ;("<tab>" . dired-subtree-toggle)
        ;("<C-tab>" . dired-subtree-cycle))
  )

(defun local/sudo-find-file (file)
  "Open FILE as root."
  (interactive "FOpen file as root: ")
  (when (file-writable-p file)
    (user-error "File is user writeable, aborting sudo"))
  (find-file (if (file-remote-p file)
                 (concat "/" (file-remote-p file 'method) ":"
                         (file-remote-p file 'user) "@" (file-remote-p file 'host)
                         "|sudo:root@"
                         (file-remote-p file 'host) ":" (file-remote-p file 'localname))
               (concat "/sudo:root@localhost:" file))))

(use-package wgrep
  :commands wgrep-change-to-wgrep-mode
  :custom
   (wgrep-auto-save-buffer t)
   (wgrep-change-readonly-file t)
   :config
   (define-key grep-mode-map "e" 'wgrep-change-to-wgrep-mode)
   (define-key grep-mode-map "C-c C-c" 'wgrep-finish-edit)
    )

(require 'windmove)

(defun local/win-move-sep-left (arg)
  "Move window splitter left."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'right))
      (shrink-window-horizontally arg)
    (enlarge-window-horizontally arg)))

(defun local/win-move-sep-right (arg)
  "Move window splitter right."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'right))
      (enlarge-window-horizontally arg)
    (shrink-window-horizontally arg)))

(defun local/win-move-sep-up (arg)
  "Move window splitter up."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'up))
      (enlarge-window arg)
    (shrink-window arg)))

(defun local/win-move-sep-down (arg)
  "Move window splitter down."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'up))
      (shrink-window arg)
    (enlarge-window arg)))

(require 'transient)
(transient-define-prefix local/win-resize ()
  "Resize windows"
  :transient-suffix     'transient--do-stay
  :transient-non-suffix 'transient--do-warn
  ["Actions"
   ("b" "balance" balance-windows :transient nil)
   ("h" "X←" local/win-move-sep-left)
   ("j" "X↓" local/win-move-sep-down)
   ("k" "X↑" local/win-move-sep-up)
   ("l" "X→" local/win-move-sep-right)])

(require 'transient)
(transient-define-prefix local/win-zoom ()
  "Zoom window"
  :transient-suffix     'transient--do-stay
  :transient-non-suffix 'transient--do-warn
  ["Actions"
   ("0" "reset" local/win-zoom-reset :transient nil)
   ("-" "-" text-scale-decrease)
   ("=" "+" text-scale-increase)])

(use-package ace-window
  :custom
   (aw-dispatch-always t)
  :init
   (setq aw-dispatch-alist
    '((?? aw-show-dispatch-help)
     (?= local/ace-window-zoom "Zoom Text")
     (?F aw-split-window-fair "Split Fair Window")
     (?M aw-move-window "Move Window")
     (?v aw-split-window-horz "Split Horz Window")
     (?c aw-copy-window "Copy Window")
     (?e aw-execute-command-other-window "Execute Command Other Window")
     (?j aw-switch-buffer-in-window "Select Buffer")
     (?m aw-swap-window "Swap Windows")
     (?n aw-flip-window)
     (?o delete-other-windows "Delete Other Windows")
     (?r local/ace-window-resize "Resize Windows")
     (?t local/ace-window-toggle-split "Toggle Windows Split")
     (?u aw-switch-buffer-other-window "Switch Buffer Other Window")
     (?b aw-split-window-vert "Split Vert Window")
     (?x aw-delete-window "Delete Window")))
   :config
   (eval-when-compile
    (defmacro local/embark-ace-action (fn)
      `(defun ,(intern (concat "local/embark-ace-" (symbol-name fn))) ()
         (interactive)
         (with-demoted-errors "%s"
           (require 'ace-window)
           (let ((aw-dispatch-always t))
             (aw-switch-to-window (aw-select nil))
             (call-interactively (symbol-function ',fn)))))))

  (defun local/ace-window-resize (window)
    "Ace-window command to resize the WINDOW."
    (aw-switch-to-window window)
    (unwind-protect
        (local/win-resize)
      (aw-flip-window)))
  (defun local/ace-window-zoom (window)
    "Ace-window command to zoom the WINDOW text."
    (aw-switch-to-window window)
    (unwind-protect
        (local/win-zoom)
      (aw-flip-window)))
  (defun local/ace-window-toggle-split (window)
    "Ace-window command to toggle the WINDOWs split."
    (aw-switch-to-window window)
    (unwind-protect
        (local/win-toggle-split)
      (aw-flip-window)))



   
  (defun local/win-zoom-reset ()
    (interactive)
    (text-scale-increase 0))

  (defun local/win-toggle-split ()
    (interactive)
    (if (= (count-windows) 2)
        (let* ((this-win-buffer (window-buffer))
               (next-win-buffer (window-buffer (next-window)))
               (this-win-edges (window-edges (selected-window)))
               (next-win-edges (window-edges (next-window)))
               (this-win-2nd (not (and (<= (car this-win-edges)
                                           (car next-win-edges))
                                       (<= (cadr this-win-edges)
                                           (cadr next-win-edges)))))
               (splitter
                (if (= (car this-win-edges)
                       (car (window-edges (next-window))))
                    'split-window-horizontally
                  'split-window-vertically)))
          (delete-other-windows)
          (let ((first-win (selected-window)))
            (funcall splitter)
            (if this-win-2nd (other-window 1))
            (set-window-buffer (selected-window) this-win-buffer)
            (set-window-buffer (next-window) next-win-buffer)
            (select-window first-win)
            (if this-win-2nd (other-window 1))))))

  (defun local/swap-up-down ()
    (interactive)
    (if (window-in-direction 'above)
        (aw-swap-window (window-in-direction 'above))
      (aw-swap-window (window-in-direction 'below))))

  (defun local/swap-left-right ()
    (interactive)
    (if (window-in-direction 'right)
        (aw-swap-window (window-in-direction 'right))
      (aw-swap-window (window-in-direction 'left))))
  )

(use-package avy
  :custom
  (avy-all-windows 'all-frames)
:config
  (defun local/avy-kill-whole-line (pt)
    (save-excursion
      (goto-char pt)
      (kill-whole-line))
    (select-window
     (cdr
      (ring-ref avy-ring 0)))
    t)

  (defun local/avy-copy-whole-line (pt)
    (save-excursion
      (goto-char pt)
      (cl-destructuring-bind (start . end)
          (bounds-of-thing-at-point 'line)
        (copy-region-as-kill start end)))
    (select-window
     (cdr
      (ring-ref avy-ring 0)))
    t)

  (defun local/avy-yank-whole-line (pt)
    (local/avy-copy-whole-line pt)
    (save-excursion (yank))
    t)

  (defun local/avy-teleport-whole-line (pt)
    (local/avy-kill-whole-line pt)
    (save-excursion (yank)) t)
  
  (defun local/avy-mark-to-char (pt)
    (activate-mark)
    (goto-char pt))
  (global-set-key (kbd "<f9>") #'avy-goto-char)
  (setf
   (alist-get ?k avy-dispatch-alist) 'avy-action-kill-stay
   (alist-get ?K avy-dispatch-alist) 'local/avy-kill-whole-line
   (alist-get ?y avy-dispatch-alist) 'avy-action-yank
   (alist-get ?w avy-dispatch-alist) 'avy-action-copy
   (alist-get ?W avy-dispatch-alist) 'local/avy-copy-whole-line
   (alist-get ?Y avy-dispatch-alist) 'local/avy-yank-whole-line
   (alist-get ?t avy-dispatch-alist) 'avy-action-teleport
   (alist-get ?T avy-dispatch-alist) 'local/avy-teleport-whole-line
   (alist-get ?  avy-dispatch-alist) 'local/avy-mark-to-char
   )
  )

(setq bookmark-save-flag 1)

(use-package org-contrib)

(use-package org
  :custom
   (org-archive-location "%s_archives::")
   (org-capture-bookmark nil)
   (org-cycle-separator-lines 2)
   (org-directory local/org-dir)
   (org-edit-src-content-indentation 2)
   (org-ellipsis " ▾")
   (org-fold-catch-invisible-edits 'smart)
   (org-fold-show-context-detail t)
   (org-fontify-quote-and-verse-blocks t)
   (org-hide-block-startup nil)
   (org-hide-emphasis-markers t)
   (org-imenu-depth 4)
   (org-mouse-1-follows-link t)
   (org-return-follows-link t)
   (org-src-fontify-natively t)
   (org-src-preserve-indentation nil)
   (org-src-tab-acts-natively t)
   (org-startup-folded 'content)
   ;; org-fold-show-context-detail '((agenda . local)
   ;;                                (bookmark-jump . lineage)
   ;;                                (isearch . lineage)
   ;; (default . canonical))
   ;; Avoid accidentally editing folded regions, say by adding text after an Org “⋯”.
  :config
  (require 'org-tempo)
  (defun local/org-mode-setup ()
    (org-indent-mode 0)
    (auto-fill-mode 0)
    (visual-line-mode 0))
    (add-hook 'org-mode-hook #'local/org-mode-setup)
    (add-hook 'auto-save-hook #'org-save-all-org-buffers)
   )

(with-eval-after-load 'org
  (setf (cdr (assoc 'file org-link-frame-setup)) 'find-file))

(with-eval-after-load 'org
  (setq org-refile-targets '((nil :maxlevel . 1)
                             (org-agenda-files :maxlevel . 2)
                             (local/resources-dir :maxlevel . 1)))

  (setq org-outline-path-complete-in-steps nil)
  (setq org-refile-use-outline-path t))

(with-eval-after-load 'ispell
  (add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
  (add-to-list 'ispell-skip-region-alist '("#\\+begin_src" . "#\\+end_src")))

(use-package org-superstar
  :after org
  :custom
  (org-superstar-remove-leading-stars t)
  (org-superstar-headline-bullets-list '("●" "■" "►" "○" "□" "▷"))
  :config
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))

(defun local/date-iso ()
  "Insert the current date, ISO format, eg. 2016-12-09."
  (interactive)
  (insert (format-time-string "%F")))

(defun local/date-iso-with-time ()
  "Insert the current date, ISO format with time, eg. 2016-12-09T14:34:54+0100."
  (interactive)
  (insert (format-time-string "%FT%T%z")))

(defun local/org-table-recalculate-all ()
  "Recalculate all values in a table."
  (interactive)
  (org-table-recalculate 'iterate))

(use-package org-appear
  :custom
  (org-hide-emphasis-markers t)
  :hook org-mode-hook)

(use-package org-roam
  :custom
   (org-roam-directory local/notes-dir)
   (org-roam-graph-viewer "/usr/bin/google-chrome-stable")
   (org-roam-completion-everywhere nil)
   (org-roam-node-display-template   (concat "${title:*} "
                                             (propertize "${tags:20}" 'face 'org-tag)))
   :config
  (org-roam-db-autosync-mode 1)
  (require 'org-roam-protocol))

(defun local/insert-roam-link ()
  "Inserts an Org-roam link."
  (interactive)
  (insert "[[roam:]]")
  (backward-char 2))

(use-package key-chord
  :config
  (key-chord-mode 1)
  (key-chord-define-global "[[" #'local/insert-roam-link))

(use-package org-roam-ui
  :init
  (local/vc-install :repo "org-roam/org-roam-ui")
  :after org-roam
  :hook org-roam-mode)

(defun local/org-roam-rg-search ()
  "Search org-roam directory using consult-ripgrep. With live-preview."
  (interactive)
  (let ((consult-ripgrep-command "rg --null --ignore-case --type org --line-buffered --color=always --max-columns=500 --no-heading --line-number . -e ARG OPTS"))
    (consult-ripgrep org-roam-directory)))

(use-package org-ql)

(use-package org-transclusion
  :custom (org-transclusion-include-first-section t))

(with-eval-after-load 'transient
  (transient-define-prefix local/notes-menu ()
    "notes menu"
    [["Commands"
      ("/" "search" local/org-roam-rg-search)
      ("a" "add alias" org-roam-alias-add)
      ("b" "switch-to-buffer" org-roam-buffer-toggle)
      ("c" "capture" org-roam-capture)
      ("f" "find" org-roam-node-find)
      ("i" "create id" org-id-get-create)
      ("n" "insert node" org-roam-node-insert)
      ("q" "ql search" org-ql-search)
      ("r" "add ref" org-roam-ref-add)]
     ["Transclude"
      ("ta" "add" org-transclusion-add)
      ("tA" "add all" org-transclusion-add-all)
      ("tl" "link" org-transclusion-make-from-link)
      ("tm" "mode" org-transclusion-mode)]
     ["Journal"
      ("jd" "date" org-roam-dailies-goto-date)
      ("jj" "journal" org-roam-dailies-capture-today)]
     ["Server"
      ("sd" "sync db" org-roam-db-sync)
      ("si" "sync id" org-roam-update-org-id-locations)
      ("sg" "graph" org-roam-graph)
      ("ss" "start" org-roam-ui-mode)]]))

(require 'ispell)
(setq ispell-program-name "hunspell")

(use-package request)
(use-package etrans
  :init
  (local/vc-install :host "gitlab.com" :repo "dpom/etrans")
  :commands (etrans-translate
            etrans-set-source
            etrans-set-target))

(require 'whitespace)

(defun local/surround (begin end open close)
  "Put OPEN at START and CLOSE at END of the region.
If you omit CLOSE, it will reuse OPEN."
  (interactive  "r\nsStart: \nsEnd: ")
  (when (string= close "")
    (setq close open))
  (save-excursion
    (goto-char end)
    (insert close)
    (goto-char begin)
    (insert open)))

(add-to-list 'insert-pair-alist (list ?\= ?\=))
(global-set-key (kbd "M-=") 'insert-pair)

(use-package org-ref
  :custom
   (bibtex-completion-bibliography local/bibliography-file)
   (bibtex-completion-library-path local/bibliography-library-path)
   (bibtex-completion-pdf-field "file")
   (org-ref-default-bibliography local/bibliography-file)
   (org-ref-pdf-directory local/bibliography-library-path))

(defun get-bibtex-from-doi (doi)
  "Get a BibTeX entry from the DOI"
  (interactive "MDOI: ")
  (let ((url-mime-accept-string "text/bibliography;style=bibtex"))
    (with-current-buffer
        (url-retrieve-synchronously
         (format "http://dx.doi.org/%s"
                 (replace-regexp-in-string "http://dx.doi.org/" "" doi)))
      (switch-to-buffer (current-buffer))
      (goto-char (point-max))
      (setq bibtex-entry
            (buffer-substring
             (string-match "@" (buffer-string))
             (point)))
      (kill-buffer (current-buffer))))
  (insert (decode-coding-string bibtex-entry 'utf-8))
  (bibtex-fill-entry))

(setq view-read-only t)

(use-package markdown-mode
  :config
  (setq markdown-command "marked")
  (dolist (face '((markdown-header-face-1 . 1.2)
                  (markdown-header-face-2 . 1.1)
                  (markdown-header-face-3 . 1.0)
                  (markdown-header-face-4 . 1.0)
                  (markdown-header-face-5 . 1.0)))
    (set-face-attribute (car face) nil :weight 'normal :height (cdr face))))

(use-package csv-mode)

(with-eval-after-load 'org
  (require 'org-capture)
  (defvar local/org-task-template
    "* TODO %?\n%a\n"
    "Template for task.")
  (defvar local/org-note-template
    "* %T\n%?\n"
    "Template for note.")
  (defvar local/org-contact-template
    "* %(org-contacts-template-name)
  :PROPERTIES:
  :ADDRESS:
  :BIRTHDAY: %^{yyyy-mm-dd}
  :EMAIL: %(org-contacts-template-email)
  :NOTE: %^{NOTE}
  :END:"
    "Template for org-contacts.")
  (setq org-capture-templates `(("c" "Contact" entry (file+headline local/backlog-file "Contacts"),
                                 local/org-contact-template
                                 :empty-lines 1 :clock-resume t :prepend t)
                                ("n" "Note" entry (file+headline local/backlog-file "Notes"),
                                 local/org-note-template
                                 :empty-lines 1 :clock-resume t :prepend t)
                                ("t" "Task" entry (file+headline local/backlog-file "Tasks"),
                                 local/org-task-template
                                 :empty-lines 1 :clock-resume t :prepend t))))

(with-eval-after-load 'org
  (setq org-latex-pdf-process
        ;; '("latexmk -pdflatex='%latex -interaction nonstopmode --shell-escape' -pdf -bibtex -f %f")
        '("pdflatex -interaction nonstopmode --shell-escape %f")
        org-latex-compiler "xelatex"
        org-export-backends '(ascii html icalendar latex md odt org)
        org-latex-default-packages-alist (quote
                                          (("T1" "fontenc" t)
                                           ("" "fixltx2e" nil)
                                           ("" "graphicx" t)
                                           ("" "longtable" nil)
                                           ("" "float" nil)
                                           ("" "wrapfig" nil)
                                           ("" "rotating" nil)
                                           ("normalem" "ulem" t)
                                           ("" "amsmath" t)
                                           ("" "textcomp" t)
                                           ("" "marvosym" t)
                                           ("" "wasysym" t)
                                           ("" "amssymb" t)
                                           ("" "minted" t)
                                           ("" "hyperref" nil)
                                           "\\tolerance=1000"))
        org-latex-listings (quote minted)
        org-latex-minted-options (quote
                                  (("fontsize" "\\footnotesize")
                                   ("linenos" "true")
                                   ("xleftmargin" "0em"))))
  )

(use-package ob-http)
(use-package ox-json)
(use-package ox-epub)
(with-eval-after-load 'org
(require 'org-src)
(setq org-src-tab-acts-natively t
      org-src-fontify-natively t
      org-src-window-setup 'split-window-right
      org-src-preserve-indentation t)
(org-babel-do-load-languages
 'org-babel-load-languages
 (append org-babel-load-languages
           '((C . t)
             (clojure . t)
             (css . t)
             (ditaa . t)
             (dot . t)
             (gnuplot . t)
             (http . t)
             (java . t)
             (js . t)
             (latex . t)
             (makefile . t)
             (org . t)
             (python . t)
             (scheme . t)
             (shell . t)
             (sqlite . t)
             (sql . t)))))

(use-package plantuml-mode
  :after org
  :mode "\\.\\(plantuml\\|puml\\)\\'"
  :custom
  (plantuml-jar-path "~/.local/lib/plantuml-1.2023.10.jar")
  (org-plantuml-jar-path plantuml-jar-path)
  (plantuml-default-exec-mode 'jar)
  :config
  (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
  (require 'ob-plantuml)
  (add-to-list 'org-babel-load-languages '(plantuml . t)))

(defun local/goto-first-header ()
  "Jump backward to the first header in the buffer."
  (interactive)
  (beginning-of-buffer)
  (org-next-visible-heading 1))

(defun local/goto-last-header ()
  "Jump forward to the last header in the buffer."
  (interactive)
  (end-of-buffer)
  (org-previous-visible-heading 1))

(defun local/goto-first-child ()
  "Jump forward to first child of this header."
  (interactive)
   (outline-show-children 1)
   (outline-next-visible-heading 1))

(with-eval-after-load 'transient
(transient-define-prefix local/header-menu ()
  "header menu"
  :transient-suffix     'transient--do-stay
  :transient-non-suffix 'transient--do-warn
  [["Cursor"
    ("j" "down" org-forward-heading-same-level)
    ("k" "up" org-backward-heading-same-level)
    ("l" "forward" local/goto-first-child)
    ("h" "backward" outline-up-heading)
    ("0" "first header" local/goto-first-header)
    ("$" "last header" local/goto-last-header)]
   ["Move"
    ("-" "sort" org-sort-entries)
    ("J" "move down" org-move-subtree-down)
    ("K" "move up" org-move-subtree-up)
    ("L" "move right" org-shiftmetaright)
    ("H" "move left" org-shiftmetaleft)]
   ["Edit"
    ("~" "toggle" org-toggle-heading)
    ("i" "new" org-insert-heading-after-current  :transient nil)]
   ["Misc"
    ("/" "search" consult-imenu)
    ("a" "show all" org-show-all)
    ("f" "toggle folding" org-cycle)
    ("F" "toggle global visibility" org-shifttab)]]))

(with-eval-after-load 'transient
(defun src-first-src ()
  "Jump backward to the first src in the buffer."
  (interactive)
  (beginning-of-buffer)
  (org-babel-next-src-block))

(defun src-last-src ()
  "Jump forward to the last src in the buffer."
  (interactive)
  (end-of-buffer)
  (org-babel-previous-src-block))

(transient-define-prefix local/src-menu ()
  "src menu"
  :transient-suffix     'transient--do-stay
  :transient-non-suffix 'transient--do-warn
  [["Cursor"
    ("h" "goto src head" org-babel-goto-src-block-head)
    ("j" "down" org-babel-next-src-block)
    ("k" "up" org-babel-previous-src-block)
    ("n" "goto named src" org-babel-goto-named-src-block)
    ("-" "first src" src-first-src)
    ("$" "last src" src-last-src)]
   ["Edit"
    ("'" "edit special" org-edit-special :transient nil)
    ("a" "header arg" org-babel-insert-header-arg)
    ("s" "split src block" org-babel-demarcate-block :transient nil)]
   ["Misc"
    ("t" "tangle" org-babel-tangle  :transient nil)
    ;; ("e" "eval" org-babel-execute-src)
    ("r" "switch to code" org-babel-switch-to-session-with-code)
    ("?" "info" org-babel-view-src-block-info)
    ("!" "check" org-babel-check-src-block)]]))

(with-eval-after-load 'transient
  (transient-define-prefix local/org-menu ()
    "org menu"
    [["Edit1"
      ("'" "edit special" org-edit-special)
      ("-" "sort" org-sort)
      ("c" "cycle bullets" org-cycle-list-bullet :transient t)
      ("i" "item" org-insert-item)
      ("ef" "footnote" org-footnote-action)
      ("eg" "tags" org-set-tags-command)
      ("eh" "heading" org-toggle-heading)]
     ["Edit2"
      ("ei" "item" org-toggle-item)
      ("el" "link"  (lambda ()
                      (interactive)
                      (call-interactively 'org-insert-link)))
      ("ep" "property" org-set-property)
      ("er" "reference" org-ref-insert-link)
      ("ew" "drawer" org-insert-drawer)
      ("ex" "id" org-id-get-create)]
     ["Misc"
                                        ;("h" "header" local/header-menu)
                                        ;("g" "send to chatgpt" gptel-send )
      ("o" "open at point" org-open-at-point)
      ("O" "jump back" org-mark-ring-goto)
                                        ;("s" "src" local/src-menu)
      ("vm" "preview latex" org-latex-preview)
      ("vi" "preview images" org-display-inline-images)]
     ["SubMenus"
      ("h" "header" local/header-menu)
      ("s" "src" local/src-menu)]
     ["Tools"
      ("ta" "archive" org-archive-subtree)
      ("tc" "recalculate" local/org-table-recalculate-all)
      ("td" "detangle" org-babel-detangle)
      ("te" "eval sexp" eval-last-sexp)
      ("tr" "refile" org-refile)
      ("tt" "tangle" org-babel-tangle)
      ("tx" "export" org-export-dispatch)]]))

(defun local/org-settings-hook ()
  (local-set-key (kbd "C-c k") 'local/org-menu))

(add-hook 'org-mode-hook #'local/org-settings-hook)

(customize-set-variable 'whitespace-action '(auto-cleanup))

(require 'eldoc)
(global-eldoc-mode 1)

(electric-pair-mode 1) ; auto-insert matching bracket

(show-paren-mode 1)    ; turn on paren match highlighting
(customize-set-variable 'show-paren-when-point-inside-paren t)
(customize-set-variable 'show-paren-when-point-in-periphery t)
(add-hook 'prog-mode-hook 'show-paren-mode)

(use-package rainbow-delimiters
  :init
  (local/vc-install :repo "Fanael/rainbow-delimiters")
  :commands rainbow-delimiters-mode
  :custom (rainbow-delimiters-max-face-count 3)
  :hook prog-mode)

(use-package highlight-indent-guides
  :hook prog-mode
  :custom
  (highlight-indent-guides-method 'character) ; use characters for indent guidesg
  (highlight-indent-guides-responsive t); highlight indentation based on current line
  (highlight-indent-guides-character ?\┆); set character
  (highlight-indent-guides-auto-enabled 'top))

(use-package aggressive-indent
  :config
  (global-aggressive-indent-mode 1)
  ;; (add-to-list 'aggressive-indent-excluded-modes 'clojure-mode)
)

(use-package compile
  :custom (compilation-scroll-output t))

(use-package hideshow
  :hook
  (prog-mode . hs-minor-mode)
  :config
  (defun local/toggle-fold ()
    (interactive)
    (save-excursion
      (end-of-line)
      (hs-toggle-hiding)))

  (defun local/hide-level ()
    (interactive)
    (hs-hide-level 1)))

(use-package fold-dwim
  :config
  (global-set-key (kbd "<f7>")      'fold-dwim-toggle)
  (global-set-key (kbd "<M-f7>")    'fold-dwim-hide-all)
  (global-set-key (kbd "<S-M-f7>")  'fold-dwim-show-all))

(use-package flycheck
  :commands (flycheck-previous-error
             flycheck-next-error
             flycheck-first-error)
  :config
  (global-flycheck-mode))

(setq-default
 ;; Skip prompt for xref find definition
 xref-prompt-for-identifier nil
)

(with-eval-after-load 'transient
  (transient-define-prefix local/jump-menu ()
      "jump menu"
      [("a" "apropos" xref-find-apropos)
       ("b" "back" xref-pop-marker-stack)
       ("d" "definition" xref-find-definitions)
       ("g" "first-error" flycheck-first-error)
       ("j" "next-ref" xref-next-line)
       ("k" "prev-ref" xref-prev-line)
       ("r" "references" xref-find-references)
       ;; ("p" "prev-error" flymake-goto-prev-error)
       ("p" "prev-error" flycheck-previous-error)
       ("o" "open" org-open-at-point)
       ;; ("n" "next-error" flymake-goto-next-error)
       ("n" "next-error" flycheck-next-error)
       ("N" "ns" cider-find-ns)]))

(use-package lsp-mode
  :commands (lsp lsp-mode lsp-deffered
             lsp-workspace-blacklist-remove)
  :config
  (use-package lsp-ui)
  (use-package lsp-treemacs)
  (add-hook 'lsp-mode-hook (lambda ()
                             (lsp-enable-which-key-integration)
                             (lsp-ui-sideline-mode 1)
                             (lsp-treemacs-sync-mode 1))))

(require 'transient)
(transient-define-prefix local/lsp-menu ()
  "lsp menu"
  [["Server"
    ("l" "start" lsp)
    ("sd" "disconnect" lsp-disconnect)
    ("sh" "describe session" lsp-describe-session)
    ("sq" "quit" lsp-workspace-shutdown)
    ("sr" "restart" lsp-workspace-restart)]
   ["Format"
    ("==" "buffer" lsp-format-buffer)
    ("=r" "region" lsp-format-region)]
   ["Toggles"
    ("ta" "modeline actions" lsp-modeline-code-actions-mode)
    ("tb" "bradcrump" lsp-headerline-breadcrumb-mode)
    ("td" "ui doc-mode" lsp-ui-doc-mode)
    ("tf" "type formating" lsp-toggle-on-type-formatting)
    ("th" "type highlight" lsp-toggle-symbol-highlight)
    ("ti" "log io" lsp-toggle-trace-io)
    ("tl" "lens-mode" lsp-lens-mode)
    ("tm" "modeline diags" lsp-modeline-diagnostics-mode)
    ("tn" "signature" lsp-toggle-signature-auto-activate)
    ("ts" "sideline" lsp-ui-sideline-mode)
    ("tt" "treemacs" lsp-treemacs-sync-mode)
    ("tu" "ui-mode" lsp-ui-mode)
    ]
   ["Jump"
    ("ja" "apropos" xref-find-apropos)
    ("jd" "declaration" lsp-find-declaration)
    ;; ("je" "error list" lsp-treemacs-errors-list)
    ("jj" "definition" lsp-find-definition)
    ("jJ" "peek definitions" lsp-ui-peek-find-definitions)
    ;; ("jh" "hierarchy" lsp-treemacs-call-hierarchy)
    ("ji" "implementation" lsp-find-implementation)
    ("jI" "peek implementations" lsp-ui-peek-find-implementation)
    ("jr" "references" lsp-find-references)
    ("jR" "peek references" lsp-ui-peek-find-references)
    ("jt" "type def" lsp-find-type-definition)
    ("jS" "peek workspace symbol" lsp-ui-peek-find-workspace-symbol)] 
   ["Help"
    ("hg" "glance symbol" lsp-ui-doc-glance)
    ("hh" "thing at point" lsp-describe-thing-at-point)
    ("hs" "signature" lsp-signature-activate)]
   ["Refactor"
    ("ro" "organize imports" lsp-organize-imports)
    ("rr" "rename" lsp-rename)]
   ["Action"
    ("aa" "code actions" lsp-execute-code-action)
    ("ah" "highlight symbol" lsp-document-highlight)
    ("al" "lens" lsp-avy-lens)]])

(require 'project)
(setq project-list-file (expand-file-name "projects" local/config-var-dir))
(setq project-switch-commands 'project-dired)

(defvar local/templates-dir "~/pers/template/templates")

(defun local/mynew (project)
  "Create PROJECT layout based on one of my templates."
  (interactive "sProject: ")
  (let ((template (completing-read "Template: "
                                   (directory-files local/templates-dir (not 'absolute) "^[^.]\\w+" ))))
 
    (shell-command (format "neil new dpom/%s %s --local/root %s" template project (file-name-concat local/templates-dir template)))))

(with-eval-after-load 'transient
  (transient-define-prefix local/project-menu ()
    "project menu"
    [("/" "search" consult-git-grep)
     ("b" "switch to buffer" project-switch-to-buffer)
     ("f" "find file" project-find-file)
     ;; ("j" "load state" project-x-window-state-load)
     ("k" "kill buffers" project-kill-buffers)
     ("n" "new" local/mynew)
     ("p" "switch" project-switch-project)
     ("r" "replace" project-query-replace-regexp)
     ;; ("w" "save state" project-x-window-state-save)
     ]))

(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                2000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-header-scroll-indicators        '(nil . "^^^^^^")
          treemacs-hide-dot-git-directory          t
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-project-follow-into-home        nil
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (when treemacs-python-executable
      (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package helpful
  :config
  (global-set-key [remap describe-command] #'helpful-command)
  (global-set-key [remap describe-key] #'helpful-key))

(use-package elisp-lint
  :init
  (local/vc-install :repo "gonewest818/elisp-lint"))

(use-package elisp-format)

(defvar local/to-test-function (lambda () "place the function to test here"))

(defun local/debug ()
"Run a testing command."
(interactive)
(let ((x (funcall local/to-test-function)))
    (with-output-to-temp-buffer "*dpom temp out*"
    (print x))))

(with-eval-after-load 'transient
(transient-define-prefix local/elisp-menu ()
  "elisp menu"
  [["Eval"
    ("eb" "buffer" eval-buffer)
    ("ef" "function" eval-defun)
    ("ee" "last sexp" eval-last-sexp)
    ("em" "macro" emacs-lisp-macroexpand)
    ("er" "region" eval-region)]
   ["Repl"
    ("rr" "ielm" ielm)
    ("rg" "debug" local/debug)]
   ["Tools"
    ("=" "check parens" check-parens)
    ("'" "jump source" org-edit-src-exit)
    ("f" "format" elisp-format-buffer)
    ("t" "test" ert)
    ("j" "jump" local/jump-menu)]]))

(use-package flycheck-clj-kondo)

(use-package clojure-mode
  :mode "\\.bb\\'"
  :custom
  (clojure-indent-style 'always-indent)
  :config
  ;; (add-hook 'clojure-mode-hook #'lsp)
  (require 'flycheck-clj-kondo))

(use-package cider
  :custom
  (cider-inject-jack-in-dependencies-at-jack-in t)
  (cider-overlays-use-font-lock t)
  (cider-preferred-build-tool 'clojure-cli)
  (cider-repl-buffer-size-limit 100000)
  (cider-repl-display-help-banner nil)
  (cider-repl-display-in-current-window nil)
  (cider-repl-pop-to-buffer-on-connect nil)
  (cider-repl-result-prefix ";; => ")
  (cider-repl-use-pretty-printing t)
  (cider-switch-to-repl-on-insert nil)
  (nrepl-use-ssh-fallback-for-remote-hosts t)
  :config
  (defun local/kit-reset ()
      (interactive)
      (cider-insert-in-repl "(reset)" t))

(defun local/cider-tap-last-and-show-debugger ()
  (interactive)
  (cider-interactive-eval "(tap> *1)")
  (shell-command "i3-msg '[title=\"Flowstorm debugger\"] scratchpad show'"))

(defun local/clj-dev-tool-bench ()
  (interactive)
  (let* ((current-ns (cider-current-ns))
         (form (cider-last-sexp))
         (clj-cmd (format "(do (require 'criterium.core) (criterium.core/quick-bench %s))" form)))
    (cider-interactive-eval clj-cmd nil nil `(("ns" ,current-ns)))))

(defun local/clj-dev-tool-profile ()
  (interactive)
  (let* ((current-ns (cider-current-ns))
         (form (cider-last-sexp))
         (clj-cmd (format "(do (require 'clj-async-profiler.core) (clj-async-profiler.core/profile %s))" form)))
	  (cider-interactive-eval clj-cmd nil nil `(("ns" ,current-ns)))
	  (shell-command "xdg-open \"file:///tmp/clj-async-profiler/results/\""))))

(defvar cljtest-error-regexp
'(cljtest "FAIL in (.+) (\\(.+\\):\\([0-9,]+\\))" 1 2))
(defvar kibit-error-regexp
'(kibit "At \\(.+\\):\\([0-9,]+\\):" 1 2))
(defvar eastwood-error-regexp
'(eastwood "Directory: \\(.+\\)" 1))

(with-eval-after-load 'compile
(add-to-list 'compilation-error-regexp-alist-alist cljtest-error-regexp)
(add-to-list 'compilation-error-regexp-alist 'cljtest)

(add-to-list 'compilation-error-regexp-alist-alist kibit-error-regexp)
(add-to-list 'compilation-error-regexp-alist 'kibit)

(add-to-list 'compilation-error-regexp-alist-alist eastwood-error-regexp)
(add-to-list 'compilation-error-regexp-alist 'eastwood))

(add-to-list 'safe-local-variable-values
            '(org-babel-clojure-backend . cider))
(add-to-list 'safe-local-variable-values
              '(cider-lein-parameters . "shadow-srv :headless :host localhost"))
(add-to-list 'safe-local-variable-values
            '(cider-ns-refresh-after-fn . "integrant.repl/resume"))
(add-to-list 'safe-local-variable-values
            '(cider-ns-refresh-before-fn . "integrant.repl/suspend"))

(use-package clj-deps-new
  :commands clj-deps-new)

(use-package html-to-hiccup
  :init
  (local/vc-install :repo "dpom/html-to-hiccup")
:commands html-to-hiccup-convert-region)

(use-package clojure-essential-ref-nov
  :commands clojure-essential-ref
  :init
  (use-package nov)
  (setq clojure-essential-ref-nov-epub-path "~/.local/share/emacs/Clojure_The_Essential_Reference_v31.epub"))

(with-eval-after-load 'transient
  (transient-define-prefix local/clojure-menu ()
  "clojure menu"
  [["Eval"
    ("ea" "all" cider-load-all-project-ns)
    ("eb" "buffer" cider-load-buffer)
    ("ee" "form before cursor" cider-eval-last-sexp)
    ("e;" "comment eval form" cider-pprint-eval-last-sexp-to-comment)
    ("eE" "print eval form" cider-pprint-eval-last-sexp)
    ("ef" "top level" cider-eval-defun-at-point)
    ("ei" "interrupt" cider-interrupt)
    ("em" "macro" cider-macroexpand-1)
    ("en" "ns" cider-eval-ns-form)
    ("er" "region" cider-eval-region)]
   ["Help"
    ("ha" "apropos" cider-apropos)
    ("hc" "classpath" cider-classpath)
    ("he" "inspect last res" cider-inspect-last-result)
    ("hh" "doc" cider-doc)
    ("hj" "javadoc" cider-javadoc)
    ("hn" "namespace" cider-browse-ns)
    ("hr" "reference book" clojure-essential-ref)
    ("hs" "spec" cider-browse-spec)]
   ["REPL"
    ("rc" "connect" cider-connect-clj)
    ("re" "inspect last eval" cider-inspect-last-result)
    ;; ("rd" "clear" cider-clear-repl-buffer)
    ("rj" "jack in" cider-jack-in)
    ("rh" "info" sesman-info)
    ("rx" "exit" sesman-quit)
    ("rs" "switch" cider-switch-to-repl-buffer)
    ("rt" "tap" local/cider-tap-last-and-show-debugger)
    ("rl" "reload" cider-ns-reload)
    ("kr" "reset" local/kit-reset)]
   ["Tools"
    ("SPC" "align" clojure-align)
    ("=" "check parens" check-parens)
    ("'" "jump source" org-edit-src-exit)
    ("b" "benchmarks" local/clj-dev-tool-bench)
    ;; ("f" "format" cljstyle)
    ("l" "lsp" local/lsp-menu)
    ("p" "profile" local/clj-dev-tool-profile)
    ("j" "jump" local/jump-menu)]])
  )

(use-package yaml-mode
  :mode "\\.ya?ml\\'")

(with-eval-after-load 'transient
  (transient-define-prefix local/yaml-menu ()
  "yaml menu"
  [("'" "jump source" org-edit-src-exit)
   ("l" "lsp" local/lsp-menu)
   ("j" "jump" local/jump-menu)]))

(use-package js2-mode
  :mode "\\.jsx?\\'"
  :custom (js2-mode-show-strict-warnings nil)
  :config
  (defun local/set-js-indentation ()
    (setq-default js-indent-level 2)
    (setq-default tab-width 2))
  
  ;; Use js2-mode for Node scripts
  (add-to-list 'magic-mode-alist '("#!/usr/bin/env node" . js2-mode))
  ;; Don't use built-in syntax checking
  
  (add-hook 'local/set-js-indentation 'js2-imenu-extras-mode)
  (add-hook 'json-mode-hook 'local/set-js-indentation))

(use-package json-mode
  :mode "\\.json\\'"
  :config
  (defun local/json-array-of-numbers-on-one-line (encode array)
    "Prints the arrays of numbers in one line."
    (let* ((json-encoding-pretty-print
            (and json-encoding-pretty-print
                 (not (loop for x across array always (numberp x)))))
           (json-encoding-separator (if json-encoding-pretty-print "," ", ")))
      (funcall encode array)))
  (advice-add 'json-encode-array :around #'local/json-array-of-numbers-on-one-line))

;; get json path
(use-package json-snatcher)

(with-eval-after-load 'transient
  (transient-define-prefix local/js-menu ()
  "js menu"
  [("'" "jump source" org-edit-src-exit)
   ("f" "format" web-beautify-js)
   ("l" "lsp" local/lsp-menu)
   ("j" "jump" local/jump-menu)]))

(use-package geiser)
(use-package geiser-guile)

(use-package vterm
:custom (vterm-max-scrollback 10000))

(require 'sh-script)
(add-hook 'after-save 'executable-make-buffer-file-executable-if-script-p)

(use-package emacsql)

(defun upcase-sql-keywords ()
  (interactive)
(save-excursion
    (dolist (keywords sql-mode-postgres-font-lock-keywords)
    (goto-char (point-min))
    (while (re-search-forward (car keywords) nil t)
        (goto-char (+ 1 (match-beginning 0)))
        (when (eql font-lock-keyword-face (face-at-point))
        (backward-char)
        (upcase-word 1)
        (forward-char))))))

(use-package sqlformat
  :init
  (local/vc-install :repo "purcell/sqlformat")
  :config
  (setq sqlformat-command 'pgformatter)
  (setq sqlformat-args '("-s2" "-g")))

(defun remove-trailing-newline (point)
(if (= (char-before point) ?\n)
    (- point 1)
    point))

(defun local/sql-format (start end)
"Formats the selected sql `sqlformat'"
(interactive "r")
(shell-command-on-region
;; beginning and end of buffer
start
(remove-trailing-newline end)
;; command and parameters
"sqlformat -k upper -r -s -"
;; output buffer
(current-buffer)
;; replace?
t
;; name of the error buffer
"*Sqlformat Error Buffer*"
;; show error buffer?
t))

(put 'sql-product 'safe-local-variable #'symbolp)
(put 'sql-sqlite-login-params 'safe-local-variable (lambda (_) t))

(with-eval-after-load 'transient
  (transient-define-prefix local/sql-menu ()
  "sql menu"
  [["Eval"
    ("eb" "buffer" sql-send-buffer)
    ("ee" "string" sql-send-string)
    ("ef" "paragraf" sql-send-paragraph)
    ("er" "region" sql-send-region)]
   ["Help"
    ("ha" "all" sql-list-all)
    ("ht" "table" sql-list-table)]
   ["REPL"
    ("rc" "connect" sql-product-interactive)
    ("rs" "switch" sql-show-sqli-buffer)
    ]
   ["Tools"
    ("'" "jump source" org-edit-src-exit)
    ("f" "format" sqlformat)
    ("l" "lsp" local/lsp-menu)
    ("j" "jump" local/jump-menu)]]))

(use-package web-mode
  :mode "(\\.\\(html?\\|ejs\\|tsx\\|jsx\\)\\'"
  :custom
  (web-mode-code-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-attribute-indent-offset 2)
  (web-mode-enable-css-colorization t)
  (web-mode-enable-auto-pairing t))

;; 1. Start the server with `httpd-start'
;; 2. Use `impatient-mode' on any buffer
(use-package skewer-mode)
(use-package impatient-mode)
(use-package rainbow-mode
  :config
  (dolist (hook '(css-mode-hook
                  html-mode-hook))
    (add-hook hook (lambda () (rainbow-mode t)))) )

(use-package web-beautify)

(with-eval-after-load 'transient
  (transient-define-prefix local/html-menu ()
  "html menu"
  [("'" "jump source" org-edit-src-exit)
   ("f" "format" web-beautify-html)
   ("i" "impatient" impatient-mode)
   ("j" "jump" local/jump-menu)
   ("l" "lsp" local/lsp-menu)
   ("s" "start server" httpd-start)]))

(use-package lsp-tailwindcss
  :init
  (setq lsp-tailwindcss-add-on-mode t))

(use-package restclient)
(use-package ob-restclient)
(use-package company-restclient)

(with-eval-after-load 'org
(add-to-list 'org-babel-load-languages '(restclient . t)) )
(with-eval-after-load 'cape
(add-to-list 'completion-at-point-functions
            (cape-company-to-capf #'company-restclient)))

(with-eval-after-load 'restclient
(defun restclient-get-var (var-name)
    (let ((buf-name (buffer-name (current-buffer)))
        (buf-point (point)))
    (restclient-get-var-at-point var-name buf-name buf-point)))

(defun restclient-elisp-result-function (args offset)
    (goto-char offset)
    (let ((form (read (current-buffer))))
    (lambda ()
        (eval form)))))

(use-package python-mode
:custom
(python-indent-guess-indent-offset-verbose nil)
(python-shell-completion-native-enable nil)
        :config
(defun local/python-shell-send-line ()
    "Send the current line to shell"
    (interactive)
    (let ((python-mode-hook nil)
        (start (point-at-bol))
        (end (point-at-eol)))
    (python-shell-send-region start end)))
(defun local/python-shell-send-buffer ()
    "Send buffer content to shell and switch to it in insert mode."
    (interactive)
    (let ((python-mode-hook nil))
    (python-shell-send-buffer)))

(defun local/python-shell-send-region (start end)
    "Send region content to shell and switch to it in insert mode."
    (interactive "r")
    (let ((python-mode-hook nil))
    (python-shell-send-region start end)))

(defun local/python-format-buffer ()
    "Bind possible python formatters."
    (interactive)
    (pcase python-formatter
    ('yapf (yapfify-buffer))
    ('black (blacken-buffer))
    (code (message "Unknown formatter: %S" code))))

(defun local/python-eval-print-last-sexp ()
    "Print result of evaluating current line into current buffer."
    (interactive)
    (let ((res (python-shell-send-string-no-output
                ;; modify to get a different sexp
                (buffer-substring (line-beginning-position)
                                (line-end-position))))
        (standard-output (current-buffer)))
    (when res
        (terpri)
        (princ res)
        (terpri))))
)

(put 'python-shell-interpreter 'safe-local-variable #'stringp)

(use-package
  jupyter)

(use-package code-cells
  :config
  (let ((map code-cells-mode-map))
    (define-key map (kbd "C-c <up>") 'code-cells-backward-cell)
    (define-key map (kbd "C-c <down>") 'code-cells-forward-cell)
    (define-key map (kbd "M-<up>") 'code-cells-move-cell-up)
    (define-key map (kbd "M-<down>") 'code-cells-move-cell-down)
    (define-key map (kbd "C-c C-c") 'code-cells-eval)
    ;; Overriding other minor mode bindings requires some insistence...
    (define-key map [remap jupyter-eval-line-or-region] 'code-cells-eval)))

(use-package micromamba)

(use-package snakemake-mode)

(with-eval-after-load 'transient
  (transient-define-prefix local/python-menu ()
  "python menu"
  [["Eval"
    ("eb" "buffer" local/python-shell-send-buffer)
    ("ee" "line" local/python-shell-send-line)
    ("e;" "line" local/python-eval-print-last-sexp)
    ("er" "region" local/python-shell-send-region)]
   ["Help"
    ("ha" "all" python-describe-at-point)]
   ["REPL"
    ("rj" "run" run-python)
    ("rs" "switch" python-shell-switch-to-shell)]
   ["Environment"
    ("na" "activate" micromamba-activate)
    ("nd" "deactivate" micromamba-deactivate)]
   ["Tools"
    ("'" "jump source" org-edit-src-exit)
    ("f" "format" local/python-format-buffer)
    ("l" "lsp" local/lsp-menu)
    ("j" "jump" local/jump-menu)]]))

(require 'nxml-mode)
(add-hook 'nxml-mode-hook #'lsp-deferred)
(add-hook 'nxml-mode-hook (lambda() (hs-minor-mode 1)))
(add-to-list 'hs-special-modes-alist
             '(nxml-mode
               "<!--\\|<[^/>]*[^/]>" ;; regexp for start block
               "-->\\|</[^/>]*[^/]>" ;; regexp for end block
               "<!--"
               nxml-forward-element
               nil))
(defun local/toggle-level ()
  "mainly to be used in nxml mode"
  (interactive)
  (hs-show-block)
  (hs-hide-level 1))
(define-key nxml-mode-map "<mouse-3>" 'local/toggle-level)

(use-package ebnf-mode
  :config
  (require 'ebnf2ps))

(defun local/c-settings ()
    (c-set-style "bsd")
    (setq indent-tabs-mode nil
          c-basic-offset 2))

(use-package cc-mode
  :config
  (add-hook 'c++-mode-hook #'local/c-settings))

(use-package arduino-mode
  :init (local/vc-install :repo "dpom/arduino-mode")
  :mode ("\\.ino\\'" . arduino-mode)
  :config
  (add-hook 'arduino-mode-hook #'local/c-settings))

(use-package irony) 
(use-package irony-eldoc)
(use-package flycheck-irony)
(use-package platformio-mode)
  ;; edit ino files with adruino mode. 
  (add-to-list 'auto-mode-alist '("\\.ino$" . arduino-mode)) 
  ;; Enable irony for all c++ files, and platformio-mode only
  ;; when needed (platformio.ini present in project root).
  (add-hook 'c++-mode-hook (lambda ()
                             (irony-mode)
                             (irony-eldoc)
                             (platformio-conditionally-enable)))

  ;; Use irony's completion functions.
  (add-hook 'irony-mode-hook
            (lambda ()
              (define-key irony-mode-map [remap completion-at-point]
                'irony-completion-at-point-async)

              (define-key irony-mode-map [remap complete-symbol]
                'irony-completion-at-point-async)

              (irony-cdb-autosetup-compile-options)))

(use-package nix-mode
  :mode ("\\.nix\\'" "\\.nix.in\\'"))

(use-package nix-drv-mode
  :ensure nix-mode
  :mode "\\.drv\\'")

(use-package nix-shell
  :ensure nix-mode
  :commands (nix-shell-unpack nix-shell-configure nix-shell-build))

(use-package nix-repl
  :ensure nix-mode
  :commands (nix-repl))

(use-package lsp-nix
  :ensure lsp-mode
  :after (lsp-mode)
  :demand t
  :custom
  (lsp-nix-nil-formatter ["nixpkgs-fmt"]))

(with-eval-after-load 'org
  (require 'org-agenda)
  (setq org-agenda-dim-blocked-tasks t
        org-agenda-span 1
        org-clock-continuously t
        org-agenda-start-on-weekday nil
        org-agenda-start-day "-0d"
        org-agenda-files  (list
                           local/plan-dir
                           local/contacts-file)
        calendar-set-date-style 'iso))

(defmacro make-goto (functionname filename)
  (let ((funsymbol (intern (concat "goto-" functionname))))
    `(defun ,funsymbol () (interactive) (find-file (expand-file-name ,filename local/plan-dir)))))

(make-goto "pers" "Pers.org")
(make-goto "work" "Work.org")
(make-find "backlog" local/backlog-file)

(defun local/get-plan (plan-file)
  (interactive
  (list (read-file-name "Plan file: " local/plan-dir)))
  (find-file (expand-file-name plan-file local/plan-dir)))

(with-eval-after-load 'org
  (customize-set-variable 'org-todo-keywords
                          '((sequence "TODO(t)" "INPROGRESS(i)" "|" "DONE(d)")
                            (sequence "WAITING(w)" "|")
                            (sequence "|" "CANCELLED(c)")))
  (customize-set-variable 'org-agenda-skip-scheduled-if-done t)
  (customize-set-variable 'org-agenda-skip-deadline-if-done  t)
  (require 'org-faces)
  (customize-set-variable 'org-todo-keyword-faces
                          '(("TODO"       :foreground "#7c7c75" :weight normal :underline t)
                            ("WAITING"    :foreground "#9f7efe" :weight normal :underline t)
                            ("INPROGRESS" :foreground "#0098dd" :weight normal :underline t)
                            ("DONE"       :foreground "#50a14f" :weight normal :underline t)
                            ("CANCELLED"  :foreground "#ff6480" :weight normal :underline t)))
  (customize-set-variable 'org-priority-faces
                          '((?A :foreground "#e45649")
                            (?B :foreground "#da8548")
                            (?C :foreground "#0098dd")))
  (custom-set-faces '(org-done ((t (:weight bold :strike-through t))))
                    '(org-headline-done ((((class color) (min-colors 16)
                                           (background dark)) (:strike-through t)))))

  )

(with-eval-after-load 'org
  (require 'org-clock)
  ;; Save the running clock and all clock history when exiting Emacs, load it on startup
  (customize-set-variable 'org-clock-persist t)
  ;; Resume clocking task on clock-in if the clock is open
  (customize-set-variable 'org-clock-in-resume t)
  ;; Change task state to INPROGRESS when clocking in
  (customize-set-variable 'org-clock-in-switch-to-state "INPROGRESS")
  ;; Separate drawers for clocking and logs
  (customize-set-variable 'org-drawers (quote ("PROPERTIES" "LOGBOOK")))
  ;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
  (customize-set-variable 'org-clock-out-remove-zero-time-clocks t)
  ;; Clock out when moving task to a done state
  (customize-set-variable 'org-clock-out-when-done t)
  ;; Enable auto clock resolution for finding open clocks
  (customize-set-variable 'org-clock-auto-clock-resolution (quote when-no-clock-is-running))
  ;; Include current clocking task in clock reports
  (customize-set-variable 'org-clock-report-include-clocking-task t)
  (customize-set-variable 'org-clock-continuously nil)
  (customize-set-variable 'org-clock-persist-file (expand-file-name (format "%s/emacs/org-clock-save.el" xdg-cache)))
  (customize-set-variable 'org-show-notification-handler (lambda (msg) (alert msg)))
  (add-hook 'kill-emacs-hook #'org-clock-save)

  (defun local/start-task ()
    (interactive)
    (let ((current-prefix-arg 4))
      (call-interactively 'org-clock-in)))
  )

(with-eval-after-load 'org
  (require 'org-contacts)
  (customize-set-variable 'org-contacts-files (list local/contacts-file)))

(transient-define-prefix local/agenda-menu ()
  "agenda menu"
  [["View"
    ("a" "agenda" org-agenda-list)
    ("D" "day" org-agenda-day-view)
    ("W" "week" org-agenda-week-view)
    ("M" "month" org-agenda-month-view)]
   ["Agenda"
    ("r" "refile" org-refile)
    ("s" "schedule" org-schedule)
    ("t" "todo" org-todo)]
   ["Clock"
    ("i" "in" org-agenda-clock-in)
    ("o" "out" org-agenda-clock-out)
    ("c" "cancel" org-clock-cancel)]
   ["Files"
    ("b" "backlog" find-backlog)
    ("f" "find plan file" local/get-plan)
    ("p" "pers" goto-pers)
    ("w" "work" goto-work)]])

(use-package ent
  :init
  (local/vc-install :host "gitlab.com" :repo "dpom/ent")
  :config
  (setq ent-default-task-number 100))

(use-package magit
  :commands magit-status
  ;; :hook
  ;; (magit-pre-refresh . diff-hl-magit-pre-refresh)
  ;; (magit-post-refresh . diff-hl-magit-post-refresh)
  )

(use-package diff-hl
  :commands (diff-hl-mode diff-hl-dired-mode)
  ;; :autoload (diff-hl-magit-pre-refresh diff-hl-magit-post-refresh)
  :hook
  ((prog-mode conf-mode) . diff-hl-mode))

(use-package smerge-mode
  :commands smerge-mode)

(local/vc-install :host "codeberg.org" :repo "pidu/git-timemachine")

(use-package dockerfile-mode
  :mode "Dockerfile\\'")

(use-package docker-compose-mode
  :mode "docker-compose.yml\\'")

(use-package ledger-mode
  :hook (ledger-mode outline-minor-mode)
  :mode "\\.lgr\\'"
  :custom
   (ledger-use-iso-dates t)
   (ledger-reports '(("bal" "%(binary) -f %(ledger-file) bal")
                    ("bal this quarter" "%(binary) -f %(ledger-file) --period \"this quarter\" bal")
                    ("bal last quarter" "%(binary) -f %(ledger-file) --period \"last quarter\" bal")
                    ("reg" "%(binary) -f %(ledger-file) reg")
                    ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
                    ("account" "%(binary) -f %(ledger-file) reg %(account)"))))

(setq auth-sources '("~/.gnupg/shared/authinfo.gpg"
                     "~/.authinfo.gpg"
                     "~/.authinfo"
                     "~/.netrc"))

(use-package pass
  :commands pass
  :custom
   (pass-show-keybindings nil)
   (pass-username-fallback-on-filename t)
   :config
   (require 'password-store)
   (defun local/pass-kill-recursiv ()
     (interactive)
     (let ((entry (pass-directory-at-point)))
       (when (yes-or-no-p (format "Do you want remove the entry %s? " entry))
         (password-store-remove entry)
         (pass-update-buffer)))))

(use-package auth-source-pass
  :config
  (auth-source-pass-enable))

(use-package pinentry
  :hook
  (after-init . pinentry-start)
  :config
  (setq epa-pinentry-mode 'loopback))

(use-package keychain-environment
  :config
  (keychain-refresh-environment))

(require 'epa-file)
(epa-file-enable)

(use-package mu4e
    :load-path "/usr/share/emacs/site-lisp/mu4e/"
    :custom
     (mu4e-maildir "~/Maildir")
     (mu4e-sent-folder "/sent" "folder for sent messages")
     (mu4e-drafts-folder "/drafts" "unfinished messages")
     (mu4e-trash-folder "/trash" "trashed messages")
     (mu4e-refile-folder "/archive"  "saved messages")
     (mu4e-get-mail-command "mbsync -V -a")
     (mu4e-update-interval (* 10 60) "update every 10 minutes")
     (mu4e-attachment-dir "~/Downloads")
     (mu4e-view-show-images t)
     (mu4e-show-images t)
     (mu4e-view-image-max-width 800)
     (mu4e-image-max-width 800)
     (mu4e-confirm-quit nil)
     (mu4e-headers-auto-update t)
     (mu4e-compose-signature-auto-include nil)
     (mu4e-sent-messages-behavior 'delete)
     (mu4e-change-filenames-when-moving t "work better for mbsync")
     (message-kill-buffer-on-exit t)
     (mu4e-compose-dont-reply-to-self t)
     (mu4e-bookmarks '((:name  "Unread messages"
                              :query "flag:unread AND NOT flag:trashed"
                              :key ?u)
                      (:name "INBOX"
                             :query "maildir:/gmail/INBOX"
                             :key ?b)
                      (:name "Today's messages"
                             :query "maildir:/gmail/today"
                             :key ?t)
                      (:name "Last week"
                             :query "maildir:/gmail/week"
                             :key ?w)
                      (:name "Last month"
                             :query "maildir:/gmail/month"
                             :key ?m)
                      (:name "Last year"
                             :query "maildir:/gmail/year"
                             :key ?a)))
     (mu4e-maildir-shortcuts '((:maildir "/gmail/INBOX" :key ?i)
                              (:maildir "/gmail/Spam"  :key ?s)
                              (:maildir "/drafts"      :key ?d)
                              (:maildir "/gmail/year"  :key ?a)
                              (:maildir "/gmail/month" :key ?m)
                              (:maildir "/gmail/week"  :key ?w)
                              (:maildir "/gmail/today" :key ?t))))
     

    (use-package mu4e-alert
      :config
      (when (executable-find "notify-send")
        (mu4e-alert-set-default-style 'libnotify))
      (add-hook 'after-init #'mu4e-alert-enable-notifications)
      (add-hook 'after-init #'mu4e-alert-enable-mode-line-display)
    ;; view
    (add-to-list 'mu4e-view-actions
                 '("open in browser" . mu4e-action-view-in-browser) t)
    ;; use imagemagick, if available
    (when (fboundp 'imagemagick-register-types)
      (imagemagick-register-types))
    
    (require 'mu4e-contrib)
    (setq mu4e-html2text-command 'mu4e-shr2text)
    (setq shr-color-visible-luminance-min 50)
    (setq shr-color-visible-distance-min 5)
    ;; (setq mu4e-html2text-command-orig "html2text2  -b 72")
    ;; (setq mu4e-html2text-command mu4e-html2text-command-orig)
    ;; (setq mail-user-agent (mu4e-user-agent))
    ;; (add-to-list 'eaf-mua-get-html
    ;;        '(("^mu4e:" . eaf-mu4e-get-html)))
    
    ;; (defcustom my-eaf-mua-get-html
    ;;   '((gnus-user-agent    . eaf-gnus-get-html)
    ;;     (mu4e-user-agent    . eaf-mu4e-get-html)
    ;;     (notmuch-user-agent . eaf-notmuch-get-html))
    ;;   "Association list with mail user agent as a KEY and a function as VALUE used
    ;; to retrieve HTML part of a mail.
    ;; The value of `mail-user-agent' has to be a KEY of `eaf-mua-get-html'."
    ;;   :type 'alist)
    
    ;; (defun my-eaf--get-html-func ()
    ;;   (if-let ((get-html (assoc-default mail-user-agent my-eaf-mua-get-html)))
    ;;       get-html
    ;;     (error "Mail User Agent \"%s\" not supported" mail-user-agent)))
    
    
    
    ;; (defun my-eaf-open-mail-as-html ()
    ;;   "Open the html mail in EAF browser.
    ;; The value of `mail-user-agent' has to be a KEY of the assoc list `eaf-mua-get-html'.
    ;; In that way the corresponding function will be called to retrieve the HTML part of
    ;; the current mail."
    ;;   (interactive)
    ;;   (when-let* ((get-html (my-eaf--get-html-func))
    ;; 	      (html (funcall get-html))
    ;; 	      (filename (concat (make-temp-name "eaf-mail-") ".html"))
    ;; 	      (file (concat (temporary-file-directory) filename)))
    ;;     (with-temp-file file
    ;;       (insert html))
    ;;     (eaf-open file "browser" "temp_html_file")))
    
    ;; ;; toggle between html2text and mu4e-shr2text
    ;; (defun mu4e-view-toggle-html2 ()
    ;;   "Toggle html-display of the message body (if any)."
    ;;   (interactive)
    ;;   (setq mu4e-html2text-command (if (equal mu4e-html2text-command 'mu4e-shr2text) 'my-eaf-open-mail-as-html 'mu4e-shr2text))
    ;;   (setq mu4e~view-html-text 'html)
    ;;   (mu4e-view-refresh)
    ;; )
    ;; (define-key mu4e-view-mode-map    (kbd "C-c h") 'mu4e-view-toggle-html2)
    
    ;; compose
    (add-hook 'mu4e-compose-mode-hook
              (defun my-do-compose-stuff ()
                "My settings for message composition."
                (set-fill-column 72)
                (flyspell-mode)))
    (setq message-send-mail-function 'smtpmail-send-it) ;; tell message-mode how to send mail
    
    
    ;; if offline queuing the messages
    (setq smtpmail-queue-mail nil  ;; start in queuing mode
          smtpmail-queue-dir   "~/Maildir/queue/cur")
    ;; spell check
    )

;;use org mode for eml files (useful for thunderbird plugin)
(add-to-list 'auto-mode-alist '("\\.eml\\'" . org-mode))

(use-package erc
  :custom
  (erc-server "irc.libera.chat")
  (erc-nick "dpom")
  (erc-user-full-name "Dan Pomohaci")
  (erc-track-shorten-start 8)
  (erc-autojoin-channels-alist '(("irc.libera.chat" "#systemcrafters" "#emacs")))
  (erc-kill-buffer-on-part t)
  (erc-auto-query 'bury)
  (erc-fill-column 120)
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 10)
  (erc-track-exclude '("#emacs"))
  (erc-track-exclude-types '("JOIN" "NICK" "QUIT" "MODE" "AWAY" "PART"))
  (erc-hide-list '("JOIN" "NICK" "QUIT" "MODE" "AWAY" "PART"))
  (erc-track-exclude-server-buffer t)
  :config
  (add-to-list 'erc-modules 'notifications)
  (erc-update-modules))

(use-package erc-hl-nicks
  :after erc
  :config
  (add-to-list 'erc-modules 'hl-nicks))

(use-package erc-image
  :after erc
  :config
  (setq erc-image-inline-rescale 300)
  (add-to-list 'erc-modules 'image))

(use-package emojify
  :hook (erc-mode . emojify-mode)
  :commands emojify-mode)

(erc-update-modules)

(use-package elfeed)

(use-package elfeed-org
  :after elfeed
  :config
    (elfeed-org)
    :custom
      (rmh-elfeed-org-files (list "~/.config/emacs/elfeed.org")))

(use-package gptel
  :init
  (local/vc-install :repo "karthink/gptel"))

(use-package gptel
  :commands gptel-send
  :config
  (require 'subr-x)
  :custom
    (gptel-model "gpt-3.5-turbo")
    (gptel-playback t)
    (gptel-default-mode 'org-mode)
    (gptel-api-key (string-trim (shell-command-to-string "pass openai.com/apikey"))))

(unless (and (fboundp 'server-running-p)
             (server-running-p))
  (server-start))

(use-package which-key
  :custom (which-key-sort-order 'which-key-prefix-then-key-order)
  :config
  (which-key-mode 1))

(use-package transient
  :config
  (transient-bind-q-to-quit))

(define-key completion-list-mode-map "n" 'next-line)
(define-key completion-list-mode-map "p" 'previous-line)
(define-key completion-list-mode-map "f" 'next-completion)
(define-key completion-list-mode-map "b" 'previous-completion)
(define-key completion-list-mode-map "M-v" 'local/focus-minibuffer)
(define-key completion-list-mode-map "?" 'local/focus-minibuffer)

(defun local/specific-menu-command ()
  "call different menus depending on what's current major mode."
  (interactive)
  (cond
   ((string-equal major-mode "org-mode") (local/org-menu))
   ((string-equal major-mode "clojure-mode") (local/clojure-menu))
   ((string-equal major-mode "elisp-mode") (local/elisp-menu))
   ((string-equal major-mode "python-mode") (local/python-menu))
   ((string-equal major-mode "web-mode") (local/html-menu))
   ((string-equal major-mode "sql-mode") (local/sql-menu))
   ((string-equal major-mode "js-mode") (local/js-menu))
   ((string-equal major-mode "json-mode") (local/js-menu))
   ((string-equal major-mode "yaml-mode") (local/yaml-menu))

   ;; if nothing match, use generic prog menu
   (t (local/prog-menu))))
(define-key mode-specific-map "\\" 'local/specific-menu-command)

(require 'transient)

(transient-define-prefix local/buffer-menu ()
  "buffer menu"
  [("b" "switch buffer" consult-buffer)
   ("B" "ibuffer" ibuffer)
   ("d" "kill current buffer" kill-this-buffer)
   ("f" "display popper" popper-toggle-latest)
   ("j" "cycle popper" popper-cycle)
   ("k" "pick & kill" kill-buffer)
   ("l" "list buffers" list-buffers)
   ("r" "rename buffer" rename-buffer)
   ("t" "toggle popper" popper-toggle-type)])

(transient-define-prefix local/prog-menu ()
  "prog menu"
  [("'" "jump source" org-edit-src-exit)
   ("l" "lsp" local/lsp-menu)
   ("j" "jump" local/jump-menu)])

(transient-define-prefix local/file-menu ()
  "file menu"
  [[("SPC" "locate" consult-locate)
    ("." "dir other window" dired-jump-other-window)
    ("/" "grep" local/search-in-files)
    ("a" "accounts" find-accounts)
    ("c" "contacts" find-contacts)]
   [("d" "dired" dired)
    ("e" "emacs config" find-config)
    ("f" "find file" find-file)
    ("l" "find library" find-library)]
   [("n" "notebook" find-notebook)
    ("p" "at point" find-file-at-point)
    ("r" "recently opened files" consult-recent-file)
    ("s" "save file" save-buffer)
    ("w" "save as" write-file)
    ]])

(transient-define-prefix local/git-menu ()
  "git menu"
  [("b" "blame" magit-blame)
   ("c" "clone" magit-clone )
   ("y" "status" magit-status)
   ("i" "init" magit-init )
   ("l" "log (current file)" magit-log-buffer-file)
   ("L" "log (project)" magit-log-current)
   ("t" "timemachine" git-timemachine)])

(transient-define-prefix local/help-menu ()
  "help menu"
  [("a" "apropos" apropos)
   ("A" "apropos doc" apropos-documentation)
   ("b" "bindings" describe-bindings)
   ("f" "function" describe-function)
   ("k" "key" describe-key)
   ("m" "mode" describe-mode)
   ("n" "minor mode" describe-minor-mode)
   ("v" "variable" describe-variable)])

(transient-define-prefix local/toggle-menu ()
  "toggle menu"
  [["General"
    ("SPC" "whitespace mode" whitespace-mode)
    ("e" "debug on error" toggle-debug-on-error)
    ("f" "folding" hs-minor-mode)
    ("q" "debug on quit" toggle-debug-on-quit)
    ("o" "set font" fontaine-set-preset)
    ("r" "read-only" read-only-mode)
    ("s" "font size" local/iw-set-font-size)
    ]
   ["Programming"
    ("p|" "indent guide" highlight-indent-guides-mode)
    ("pa" "aggresive indent" aggressive-indent-mode)
    ("pn" "line number" display-line-numbers-mode)
    ("pm" "smerge" smerge-mode)
    ("pl" "lsp-ui" lsp-ui-mode)
    ;; ("pd" "side dir" dirvish-side)
    ]
   ["Writing"
    ;; ("wd" "dest lang" etrans-set-target)
    ("wi" "input method" set-input-method)
    ;; ("ws" "src lang" etrans-set-source)
    ("wt" "truncate lines" toggle-truncate-lines)
    ("wy" "flyspel" flyspell-mode)]])

(define-key mode-specific-map "+" 'etrans-translate)
(define-key mode-specific-map "'"  'org-capture)
(define-key mode-specific-map "-"  'sort-lines)
(define-key mode-specific-map "/"  'consult-line)
(define-key mode-specific-map "C"  'calendar)
(define-key mode-specific-map "E"  'eshell)
(define-key mode-specific-map "G"  'gptel)
(define-key mode-specific-map "I"  'erc-tls)
(define-key mode-specific-map "M"  'mu4e) 
(define-key mode-specific-map "T"  'vterm)
(define-key mode-specific-map "W"  'pass)
(define-key mode-specific-map "a"  'local/agenda-menu)
(define-key mode-specific-map "b"  'local/buffer-menu)
(define-key mode-specific-map "d"  'local/date-iso)
(define-key mode-specific-map "f"  'local/file-menu)
(define-key mode-specific-map "i"  'consult-imenu) 
(define-key mode-specific-map "j"  'local/jump-menu)
(define-key mode-specific-map "l"  'org-store-link)
(define-key mode-specific-map "n"  'local/notes-menu)
(define-key mode-specific-map "o"  'org-clock-out) 
(define-key mode-specific-map "p"  'local/project-menu)
(define-key mode-specific-map "s"  'local/start-task)
(define-key mode-specific-map "t"  'local/toggle-menu)
(define-key mode-specific-map "u"  'org-clock-in-last)
(define-key mode-specific-map "w"  'ace-window)
(define-key mode-specific-map "y"  'local/git-menu)
(define-key mode-specific-map "z"  'ent)

(let ((normal-keybindings '(("<escape>" "ignore") ("`" "local/surround") ("!" "bookmark-set") ("@" "bookmark-jump") ("#" "meow-comment") ("$" "repeat") ("%" "meow-query-replace") ("&" "meow-query-replace-regexp") ("'" "repeat") ("(" "meow-expand-1") (")" "meow-expand-2") ("*" "goto-last-change") ("+" "meow-expand-4") ("/" "meow-search") ("=" "indent-region") ("?" "meow-cheatsheet") ("[" "meow-beginning-of-thing") ("]" "meow-end-of-thing") ("{" "meow-expand-5") ("}" "meow-expand-3") ("\\" "local/specific-menu-command") (1 "meow-expand-1") (2 "meow-expand-2") (3 "meow-expand-3") (4 "meow-expand-4") (5 "meow-expand-5") (6 "meow-expand-6") (7 "meow-expand-7") (8 "meow-expand-8") (9 "meow-expand-9") (0 "meow-expand-0") ("-" "negative-argument") (";" "meow-reverse") ("," "meow-inner-of-thing") ("." "meow-bounds-of-thing") ("<" "meow-beginning-of-thing") (">" "meow-end-of-thing") ("a" "meow-append") ("A" "meow-open-below") ("b" "meow-back-word") ("B" "meow-back-symbol") ("c" "meow-change") ("d" "meow-delete") ("D" "meow-backward-delete") ("e" "meow-next-word") ("E" "meow-next-symbol") ("f" "meow-find") ("g" "meow-cancel-selection") ("G" "meow-grab") ("h" "meow-left") ("H" "meow-left-expand") ("i" "meow-insert") ("I" "meow-open-above") ("j" "meow-next") ("J" "meow-next-expand") ("k" "meow-prev") ("K" "meow-prev-expand") ("l" "meow-right") ("L" "meow-right-expand") ("m" "meow-join") ("n" "meow-search") ("o" "meow-block") ("O" "meow-to-block") ("p" "meow-yank") ("P" "meow-yank-pop") ("q" "meow-quit") ("Q" "meow-goto-line") ("r" "meow-replace") ("R" "meow-swap-grab") ("s" "meow-kill") ("t" "meow-till") ("u" "meow-undo") ("U" "meow-undo-in-selection") ("v" "meow-visit") ("w" "meow-mark-word") ("W" "meow-mark-symbol") ("x" "meow-line") ("X" "meow-goto-line") ("y" "meow-save") ("z" "meow-pop-selection")))
      (motion-keybindings '(("<escape>" "ignore") ("j" "meow-next") ("k" "meow-prev")))
      (leader-keybindings '((1 "meow-digit-argument") (2 "meow-digit-argument") (3 "meow-digit-argument") (4 "meow-digit-argument") (5 "meow-digit-argument") (6 "meow-digit-argument") (7 "meow-digit-argument") (8 "meow-digit-argument") (9 "meow-digit-argument") (0 "meow-digit-argument") ("?" "meow-keypad-describe-key") ("e" "dispatch: C-x C-e"))))
(defun meow-setup ()
  (let ((parse-def (lambda (x)
                     (cons (format "%s" (car x))
                           (if (string-prefix-p "dispatch:" (cadr x))
                               (string-trim (substring (cadr x) 9))
                             (intern (cadr x)))))))
    (apply #'meow-normal-define-key (mapcar parse-def normal-keybindings))
    (apply #'meow-motion-overwrite-define-key (mapcar parse-def motion-keybindings))
    (apply #'meow-leader-define-key (mapcar parse-def leader-keybindings))))
)

(use-package meow
  :custom
  (meow-char-thing-table '((?\( . round)
                           (?\[ . square)
                           (?\{ . curly)
                           (?\" . string)
                           (?e . symbol)
                           (?w . window)
                           (?b . buffer)
                           (?p . paragraph)
                           (?l . line)
                           (?v . visual-line)
                           (?d . defun)
                           (?. . sentence)))
  :config
;; hide lighters
(diminish 'meow-normal-mode)
(diminish 'meow-motion-mode)
(diminish 'meow-insert-mode)
(diminish 'meow-keypad-mode)
(diminish 'meow-beacon-mode)

;; custom indicator
(when window-system
  (setq meow-replace-state-name-list
        '((normal . "🅝")
          (beacon . "🅑")
          (insert . "🅘")
          (motion . "🅜")
          (keypad . "🅚"))))

;; custom variables
(setq meow-esc-delay 0.001)

(meow-thing-register 'angle
                   '(pair ("<") (">"))
                   '(pair ("<") (">")))

(add-to-list 'meow-char-thing-table
             '(?< . angle))

(add-to-list 'meow-mode-state-list
             '(cargo-process-mode . motion))
(add-to-list 'meow-mode-state-list
             '(emms-playlist-mode . motion))
 
(meow-setup)
(meow-setup-indicator)
(meow-global-mode 1)
)

(defun spray-mode-usr2-handler ()
      "Handle case where spray mode timer is left running when the w3m buffer it is spraying is killed inadvertently instead of stopping spray mode first and won't
respond to C-g or other mechanisms. This will stop spray mode via an
external signal: pkill -USR2 emacs."
      (interactive)
      ;; arbitrary elisp you wish to execute:
      (message "Got USR2 signal")
      (spray-stop)) 
    (global-set-key [signal usr2] 'spray-mode-usr2-handler)
    ;;               ^ here we register the event handler that will automatically be called when send-usr2-signal-to-emacs fires

    (defun send-usr2-signal-to-emacs ()
      "Send pkill -USR2 emacs rather than having to do it at the command line."
      (interactive)
      (signal-process (emacs-pid) 'sigusr2))
    (global-set-key (kbd "<f8>") 'send-usr2-signal-to-emacs)
