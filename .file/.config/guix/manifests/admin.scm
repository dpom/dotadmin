(specifications->manifest
 '(
"bash-completion"
"curl"
"direnv"
"git"
"gnupg"
"keychain"
"mc"
"stow"
"syncthing-gtk"
"tar"
"unzip"
"wget"
"arandr"
"autorandr"
"i3-wm"
"i3blocks"
"i3lock"
"i3status"
"rofi"
"stow"
"setxkbmap"
"volumeicon"
"docker"
"docker-compose"
"awscli"
      ))
