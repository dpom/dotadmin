;; [[file:../../../../Emacs.org::*Guix][Guix:1]]
(specifications->manifest
 '(
   "emacs"
   "emacs-guix"
   "cmake" ; for vterm
   "emacs-desktop-environment"
   "font-iosevka-comfy"
   "font-adobe-source-code-pro"
   "fd"
   "imagemagick"
   "mediainfo"
   "poppler"
   "ripgrep"
   "the-silver-searcher"
   "plantuml"
   "texlive"
   "pandoc"
   "aspell"
   "aspell"
   "aspell-dict-ro"
   "aspell-dict-fr"
   "aspell-dict-en"
   "python-sqlparse"
   "sqls"
   "isync"
   "ledger"
   "mu"
   "password-store"
   "w3m"
   "wget"
   ))
;; Guix:1 ends here
