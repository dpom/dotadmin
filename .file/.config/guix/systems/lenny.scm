(define-module (lenny)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mtools)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xorg)
  #:use-module (gnu services desktop)
  #:use-module (gnu services docker)
  #:use-module (gnu services networking)
  #:use-module (gnu services pm)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services)
  #:use-module (gnu system nss)
  #:use-module (gnu)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:use-module (srfi srfi-1)
  )

(use-service-modules nix)
(use-service-modules
 cups
 desktop
 linux
 networking
 ssh
 xorg)

(use-package-modules linux)
(use-package-modules certs)
(use-package-modules shells)


;; Use the libinput driver for all input devices since it’s a bit more modern than the default.
(define %xorg-libinput-config
  "Section \"InputClass\"
  Identifier \"Touchpads\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsTouchpad \"on\"

  Option \"Tapping\" \"on\"
  Option \"TappingDrag\" \"on\"
  Option \"DisableWhileTyping\" \"on\"
  Option \"MiddleEmulation\" \"on\"
  Option \"ScrollMethod\" \"twofinger\"
EndSection
Section \"InputClass\"
  Identifier \"Keyboards\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsKeyboard \"on\"
EndSection")

(operating-system
 (host-name "lenny")

 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))

 (locale "en_US.utf8")
 (timezone "Europe/Bucharest")
 (keyboard-layout (keyboard-layout "us" "altgr-intl" #:model "thinkpad"))

 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (targets (list "/dev/sda"))
              (keyboard-layout keyboard-layout)))

 (swap-devices (list (swap-space
                      (target (uuid
                               "0c49a5b5-60bb-440a-a3b9-d02d67b076cf")))))

 (file-systems (cons* (file-system
                       (mount-point "/")
                       (device (uuid
                                "c0e96b60-7d65-4a0e-a041-05f18232cb61"
                                'ext4))
                       (type "ext4"))
                      %base-file-systems))
 
 (packages
  (append
   (map specification->package
        '(
          "arandr"
          "autorandr"
          "blueman"
          "bluez"
          "bluez-alsa"
          "dbus"
          "dmenu"
          "exfat-utils"
          "fuse-exfat"
          "git"
          "gvfs"  ; for user mounts
          "i3-wm"
          "i3lock"
          "i3status"
          "nss-certs" ; for HTTPS access
          "ntfs-3g"
          "pulseaudio"
          "rofi"
          "setxkbmap"
          "st"
          "tlp"
          "xf86-input-libinput"
          ))
   %base-packages))

 (services
  (append
   (list
    (simple-service 'add-extra-hosts
                    hosts-service-type
                    (list (host "192.168.0.100" "archie")))
    (service xfce-desktop-service-type)
    (service bluetooth-service-type)
    ;; To configure OpenSSH, pass an 'openssh-configuration'
    ;; record as a second argument to 'service' below.
    (service openssh-service-type)
    (service tor-service-type)
    (service cups-service-type)
    (service docker-service-type)
    (service nix-service-type)
    (service syncthing-service-type
             (syncthinng-configuration (user "dan")))
    (extra-special-file "/usr/bin/env"
                        (file-append coreutils "/bin/env"))
    (set-xorg-configuration
     (xorg-configuration (keyboard-layout keyboard-layout))))
   %desktop-services))

 ;; Allow resolution of '.local' host names with mDNS
 (name-service-switch %mdns-host-lookup-nss)

 (users (cons* (user-account
                (name "dan")
                (comment "Dan Pomohaci")
                (group "users")
                (home-directory "/home/dan")
                (supplementary-groups
                 '(
                   "audio"     ;; control audio devices
                   "docker"
                   "input"
                   "kvm"
                   "lp"        ;; control bluetooth devices
                   "netdev"    ;; network devices
                   "tty"
                   "video"  ;; control video devices
                   "wheel"     ;; sudo
                   )))
               %base-user-accounts))
 )
